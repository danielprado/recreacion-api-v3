const getters = {
  getToken: (state) => {
    return state.access_token ? `${state.token_type} ${state.access_token}` : null
  },
  isLoggedIn: (state) => state.access_token,
  getUserData: ( state ) =>  state,
  getPermissions: (state) => state.permissions
};

export default getters;
