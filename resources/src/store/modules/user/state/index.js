export default {
  access_token: null,
  token_type: null,
  expires_in: null,
  refresh_token: null,
  id: null,
  name: null,
  email: null,
  username: null,
  sim_id: null,
  document: null,
  document_type_id: null,
  document_type: null,
  document_type_description: null,
  full_name: null,
  part_name: null,
  lastname: null,
  second_lastname: null,
  first_name: null,
  second_name: null,
  birthday: null,
  country_id: null,
  city: null,
  gender_id: null,
  ethnicity_id: null,
  department_id: null,
  gender: null,
  country: null,
  ethnicity: null,
  permissions: [],
};
