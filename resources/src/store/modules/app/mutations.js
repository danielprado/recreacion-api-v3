import { set, toggle } from '@/utils/vuex'

export default {
  setSheet: set('sheet'),
  setFullScreenDialog: set('fullScreenDialog'),
  setLang: set('lang'),
  setDrawer: set('drawer'),
  setSnack: set('snack'),
  setSnackMessage: set('snack_message'),
  setSnackIcon: set('snack_icon'),
  setSnackPosition: set('snack_position'),
  setSnackType: set('snack_type'),
  setDrawerRight: set('drawerRight'),
  setImage: set('image'),
  setColor: set('color'),
  setDrawerColor: set('sidebarBackgroundColor'),
  toggleDrawer: toggle('drawer'),
  toggleLoading: toggle('pageLoading'),
  toggleDrawerRight: toggle('drawerRight'),
  toggleMini: toggle('mini'),
  toggleDark: toggle('dark'),
  toggleRTL: toggle('rtl')
}
