let image = (process.env.NODE_ENV === 'production')
  ? `${process.env.VUE_APP_API_URL_PROD}/public/img/sidebar-1.61c4c50e.jpg`
  : `${process.env.VUE_APP_API_URL_DEV}${process.env.VUE_APP_PUBLIC_PATH_DEV}img/sidebar-1.61c4c50e.jpg`;

export default {
  sheet: null,
  fullScreenDialog: null,
  lang: 'es',
  drawer: null,
  snack: null,
  snack_message: null,
  snack_icon: 'mdi-bell-plus',
  snack_position: 'bottom',
  snack_type: 'black',
  drawerRight: false,
  color: 'info',
  rtl: false,
  mini: false,
  dark: false,
  pageLoading: false,
  image: image,
  sidebarBackgroundColor: 'rgba(0, 0, 0, 0), rgba(0, 0, 0, 0)'
}
