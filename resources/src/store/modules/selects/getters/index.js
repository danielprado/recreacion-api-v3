const getters = {
  getDocumentTypes: (state) => state.document_types,
  getGenders: (state) => state.genders,
  getEthnicities: (state) => state.ethnicities,
  getCountries: (state) => state.countries,
  getPrograms: (state) => state.programs,
};

export default getters;