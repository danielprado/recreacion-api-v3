const mutations = {
  STORE_DOCUMENT_TYPES(state, response) {
    state.document_types = response
  },
  STORE_GENDERS(state, response) {
    state.genders = response
  },
  STORE_ETHNICITIES(state, response) {
    state.ethnicities = response
  },
  STORE_COUNTRIES(state, response) {
    state.countries = response
  },
  STORE_PROGRAMS(state, response) {
    state.programs = response
  },

  CLEAR_DOCUMENT_TYPES(state) {
    state.document_types = []
  },
  CLEAR_GENDERS(state) {
    state.genders = []
  },
  CLEAR_ETHNICITIES(state) {
    state.ethnicities = []
  },
  CLEAR_COUNTRIES(state) {
    state.countries = []
  },
  CLEAR_PROGRAMS(state) {
    state.programs = []
  },
  
  CLEAR_ALL( state ) {
    state.document_types = [];
    state.genders = [];
    state.ethnicities = [];
    state.countries = [];
    state.programs = [];
  }
};

export default mutations
