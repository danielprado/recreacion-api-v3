import {Gender} from "@/models/services/Security/Gender";
import {DocumentType} from "@/models/services/Security/DocumentType";
import {Ethnicity} from "@/models/services/Security/Ethnicity";
import {Country} from "@/models/services/Security/Country";
import {Program} from "@/models/services/Scheme/Program";

const actions = {
  document_types: ({commit}) => {
    return new Promise( (resolve, reject) => {
      (new DocumentType()).index()
        .then(response => {
          let data = response.data.map( data =>  {
            return {
              id: data.id,
              name: `${data.name} ${data.description}`
            }
          });
          commit('STORE_DOCUMENT_TYPES', data);
          resolve( data );
        })
        .catch(error => reject( error ))
    })
  },

  genders: ({commit}) => {
    return new Promise( (resolve, reject) => {
      (new Gender()).index()
        .then(response => {
          commit('STORE_GENDERS', response.data);
          resolve( response.data );
        })
        .catch(error => reject( error ))
    })
  },

  ethnicities: ({commit}) => {
    return new Promise( (resolve, reject) => {
      (new Ethnicity()).index()
        .then(response => {
          commit('STORE_ETHNICITIES', response.data);
          resolve( response.data );
        })
        .catch(error => reject( error ))
    })
  },

  countries: ({commit}) => {
    return new Promise( (resolve, reject) => {
      (new Country()).index()
        .then(response => {
          commit('STORE_COUNTRIES', response.data);
          resolve( response.data );
        })
        .catch(error => reject( error ))
    })
  },

  programs: ({commit}) => {
    return new Promise((resolve, reject) => {
      (new Program()).index()
        .then(response => {
          commit('STORE_PROGRAMS', response.data);
          resolve( response.data );
        })
        .catch(error => reject( error ))
    })
  },
  
  clear_document_types: ({commit}) => {
    return new Promise((resolve, reject) => {
      commit('CLEAR_DOCUMENT_TYPES');
      resolve();
    })
  },

  clear_genders: ({commit}) => {
    return new Promise((resolve, reject) => {
      commit('CLEAR_GENDERS');
      resolve();
    })
  },

  clear_ethnicities: ({commit}) => {
    return new Promise((resolve, reject) => {
      commit('CLEAR_ETHNICITIES');
      resolve();
    })
  },

  clear_countries: ({commit}) => {
    return new Promise((resolve, reject) => {
      commit('CLEAR_COUNTRIES');
      resolve();
    })
  },

  clear_programs: ({commit}) => {
    return new Promise((resolve, reject) => {
      commit('CLEAR_PROGRAMS');
      resolve();
    })
  },

  clear_all: ({commit}) => {
    return new Promise((resolve, reject) => {
      commit('CLEAR_ALL');
      resolve();
    })
  }
  
};

export default actions;