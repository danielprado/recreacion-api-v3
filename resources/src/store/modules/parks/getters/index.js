const getters = {
  getParks: (state) => state.parks
};

export default getters;