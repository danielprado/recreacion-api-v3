/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
// mdi-view-dashboard
import Vue from 'vue'
import User from "@/package/user";
import Auth from "@/package/auth";
Vue.use(User);
Vue.use(Auth);
import {permissions} from "@/utils/permissions";

export default [
    {
        path: '/security',
        name: 'Security',
        icon: 'mdi-lock',
        props: true,
        can: Vue.auth.can( permissions.security ),
        children: [
            {
                path: 'users',
                name: 'Users',
                view: 'Users',
                icon: 'mdi-account-multiple',
                props: true,
                can: Vue.auth.can( permissions.users ),
            },
            {
                path: 'roles',
                name: 'Roles',
                view: 'Roles',
                icon: 'mdi-shield-account',
                props: true,
                can: Vue.auth.can( permissions.roles ),
            },
            {
                path: 'permissions',
                name: 'Permissions',
                view: 'Permissions',
                icon: 'mdi-clipboard-list',
                props: true,
                can: Vue.auth.can( permissions.permissions ),
            },
            {
                path: 'assign-permissions',
                name: 'AssignPermissions',
                view: 'AssignPermissions',
                icon: 'mdi-playlist-plus',
                props: true,
                can: Vue.auth.can( permissions.assign_permissions ),
            },
            {
                path: 'assign-roles',
                name: 'AssignRoles',
                view: 'AssignRoles',
                icon: 'mdi-security',
                props: true,
                can: Vue.auth.can( permissions.assign_role ),
            },
            {
                path: 'programs-schemes',
                name: 'ProgramsScheme',
                view: 'ProgramsScheme',
                icon: 'mdi-format-list-numbered',
                props: true,
                can: Vue.auth.can( permissions.schema_activities ),
            },
            {
                path: 'programs-schemes/:id/activities',
                name: 'ActivitiesScheme',
                view: 'ActivitiesScheme',
                icon: 'mdi-format-list-numbered',
                hideInMenu: true,
                props: true,
                can: Vue.auth.can( permissions.schema_activities ),
            },
            {
                path: 'programs-schemes/:program_id/activities/:activity_id/sub-activities',
                name: 'SubActivitiesScheme',
                view: 'SubActivitiesScheme',
                icon: 'mdi-format-list-numbered',
                hideInMenu: true,
                props: true,
                can: Vue.auth.can( permissions.schema_activities ),
            },
            {
                path: 'programs-schemes/:program_id/activities/:activity_id/sub-activities/:sub_activity_id/thematics',
                name: 'ThematicsScheme',
                view: 'ThematicsScheme',
                icon: 'mdi-format-list-numbered',
                hideInMenu: true,
                props: true,
                can: Vue.auth.can( permissions.schema_activities ),
            },
            {
                path: 'programs-schemes/:program_id/activities/:activity_id/sub-activities/:sub_activity_id/thematics/:thematic_id/components',
                name: 'ComponentsScheme',
                view: 'ComponentsScheme',
                icon: 'mdi-format-list-numbered',
                hideInMenu: true,
                props: true,
                can: Vue.auth.can( permissions.schema_activities ),
            },
            {
                path: 'programs-schemes/:program_id/activities/:activity_id/sub-activities/:sub_activity_id/thematics/:thematic_id/components/:component_id/strategies',
                name: 'StrategiesScheme',
                view: 'StrategiesScheme',
                icon: 'mdi-format-list-numbered',
                hideInMenu: true,
                props: true,
                can: Vue.auth.can( permissions.schema_activities ),
            },
            {
                path: 'locations-schemes',
                name: 'LocationScheme',
                view: 'LocationScheme',
                icon: 'mdi-city',
                props: true,
                can: Vue.auth.can( permissions.scheme_locations ),
            },
            {
                path: 'locations-schemes/:id/upz',
                name: 'UpzScheme',
                view: 'UpzScheme',
                icon: 'mdi-city',
                hideInMenu: true,
                props: true,
                can: Vue.auth.can( permissions.scheme_locations ),
            },
            {
                path: 'locations-schemes/:location_id/upz/:upz_id/neighborhoods',
                name: 'NeighborhoodsScheme',
                view: 'NeighborhoodsScheme',
                icon: 'mdi-city',
                hideInMenu: true,
                props: true,
                can: Vue.auth.can( permissions.scheme_locations ),
            },
            {
              path: 'population-schemes',
              name: 'PopulationScheme',
              view: 'PopulationScheme',
              icon: 'mdi-account-multiple',
              props: true,
              can: Vue.auth.can( permissions.scheme_population ),
            },
            {
              path: 'population-schemes/:id/specific',
              name: 'SpecificPopulationScheme',
              view: 'SpecificPopulationScheme',
              icon: 'mdi-account-multiple',
              hideInMenu: true,
              props: true,
              can: Vue.auth.can( permissions.scheme_population ),
            },
        ]
    },
    {
        path: '/home',
        name: 'Home',
        view: 'Home',
        icon: 'mdi-calendar',
        can: true,
        meta: {
            hideFooter: true,
            isFullCalendar: true
        }
    },
  /*
    {
        path: '/general-calendar',
        name: 'General',
        view: 'General',
        icon: 'mdi-calendar-month-outline',
        can: true,
        meta: {
            hideFooter: true,
            isFullCalendar: true
        }
    },
    {
        path: '/citizen-attention',
        name: 'CitizenAttention',
        view: 'CitizenAttention',
        icon: 'mdi-nature-people',
        can: true
    },
    {
        path: '/citizen-attention/:id/details',
        name: 'CitizenAttentionDetails',
        view: 'CitizenAttentionDetails',
        icon: 'mdi-nature-people',
        hideInMenu: true,
        props: true,
        can: true
    },
    {
        path: '/programmings',
        name: 'Programming',
        view: 'Programming',
        icon: 'mdi-format-list-bulleted',
        can: true
    },
    {
        path: '/programmings/:id/details',
        name: 'ProgrammingDetails',
        view: 'ProgrammingDetails',
        icon: 'mdi-format-list-bulleted',
        hideInMenu: true,
        props: true,
        can: true
    },
    {
        path: '/reports',
        name: 'GeneralReport',
        icon: 'mdi-file-document-outline',
        props: true,
        can: true,
        children: [
            {
                path: 'programmings',
                name: 'GeneralReportProgramming',
                view: 'GeneralReport',
                icon: 'mdi-file-document-outline',
                props: true,
                can: true,
            },
            {
                path: 'citizen-attention',
                name: 'CalendarAttention',
                view: 'CalendarAttention',
                icon: 'mdi-file-document-outline',
                props: true,
                can: true,
            },
        ]
    },
    {
        path: '/programmings/:id/execution',
        name: 'Execution',
        view: 'Execution',
        icon: 'mdi-format-list-bulleted',
        hideInMenu: true,
        props: true,
        can: true
    },
    {
        path: '/stats',
        name: 'Stats',
        view: 'Stats',
        icon: 'mdi-chart-bar',
        can: true,
        meta: {
            hideFooter: true,
            isFullCalendar: true
        }
    },
   */
]
