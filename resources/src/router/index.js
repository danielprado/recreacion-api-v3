/**
 * Vue Router
 *
 * @library
 *
 * https://router.vuejs.org/en/
 */

// Lib imports
import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'


// Routes
import paths from './paths'
import landing from './landing'

//Layouts
import DashboardLayout from '@/components/layouts/Dashboard.vue'
import LandingLayout from '@/components/layouts/Landing.vue'
import ErrorLayout from '@/components/layouts/ErrorLayout.vue'

function routeWithChildren( main, child ) {
  return {
    name: child.name,
    path: `${main.path}/${child.path}`,
    component: (resovle) => import(`@/views/dashboard/${child.view}.vue`).then(resovle),
    props: true,
    meta: {
      requiresAuth: true,
      can: child.can,
      ...child.meta
    }
  }
}


function route ( route ) {
  if ( route.children ) {
    return {
      name: route.name,
      path: route.path,
      redirect: `${route.path}/${route.children[0].path}`,
      props: true,
      meta: {
        requiresAuth: true,
        can: route.can,
        ...route.meta
      }
    };
  } else {
    return {
      name: route.name,
      path: route.path,
      component: (resovle) => import(`@/views/dashboard/${route.view}.vue`).then(resovle),
      props: true,
      meta: {
        requiresAuth: true,
        can: route.can,
        ...route.meta
      }
    }
  }
}

function landingRoute (route) {
  return {
    name: route.name,
    path: route.path,
    component: (resovle) => import(`@/views/pages/${route.view}.vue`).then(resovle),
    meta: {
      forVisitors: true,
      can: route.can || true,
      ...route.meta
    }
  }
}

let routes = paths.map(path => route(path));
let childs = [];
paths.map(path => {
  if ( path.children ) {
    childs = path.children.map( child => routeWithChildren(path, child) )
  }
});

Vue.use(Router)

// Create a new router
const router = new Router({
  routes: [
    {
      path: '/',
      redirect: 'login',
      component: LandingLayout,
      children: landing.map(path => landingRoute(path))
    },
    {
      path: '/dashboard',
      redirect: 'home',
      component: DashboardLayout,
      children: routes.concat( childs )
    },
    /** Important, leave this route always at the end of the list to handle 404, 500 or another http errors **/
    {
      path: "/error",
      name: 'Error',
      component: ErrorLayout,
      props: true
    },
    {
      path: "**",
      redirect: "/error",
    },
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    if (to.hash) {
      return { selector: to.hash }
    }
    return { x: 0, y: 0 }
  }
});

// Routes middleware
import store from "@/store";

const waitForStorageToBeReady = async (to, from, next) => {
  await store.restored;
  if (typeof window.source == "function") {
    window.source("Canceled by the user");
  }
  if (to.matched.some(record => record.meta.forVisitors)) {
    if (Vue.auth.isLoggedIn()) {
      next({ name: "Home" });
    } else if (to.matched.some(record => record.meta.can)) {
      next();
    } else {
      next({ name: "Home" });
    }
  } else if (to.matched.some(record => record.meta.requiresAuth)) {
    if (Vue.auth.isNotLoggedIn()) {
      next({ name: "Login" });
    } else if (to.matched.some(record => record.meta.can)) {
      next();
    } else {
      next({ name: "Home" });
    }
  } else {
    next();
  }
};

// Routes middleware
router.beforeEach(waitForStorageToBeReady)

Vue.use(Meta);

export default router
