/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
import Vue from 'vue'
import User from "@/package/user";
import Auth from "@/package/auth";
Vue.use(User);
Vue.use(Auth);

export default [
    {
        path: '/login',
        name: 'Login',
        view: 'Login',
        icon: 'mdi-fingerprint',
        hideInMenu: Vue.auth.isLoggedIn(),
        meta: {
            isInset: true,
            absoluteFooter: true,
            hideFooter: false,
        }
    },
    {
        path: '/lock',
        name: 'Lock',
        view: 'Lock',
        icon: 'mdi-lock',
        hideInMenu: true,
        meta: {
            forVisitors: undefined,
            requiresAuth: true,
            isInset: true,
            absoluteFooter: true,
            hideFooter: false,
        }
    },
]
