import '@/assets/styles/index.scss'
import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import es from 'vuetify/es5/locale/es';
import en from 'vuetify/es5/locale/en';
import theme from './theme';
import TownIcon from '@/components/icons/TownHall';
import Es from '@/components/icons/Es';
import En from '@/components/icons/En';
import FlipHorizontal from '@/components/icons/FlipHorizontal';
import FlipVertical from '@/components/icons/FlipVertical';
import Bogota from '@/components/icons/Bogota';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
      options: {
        customProperties: true,
      },
    themes: theme,
  },
    lang: {
      locales: { es, en },
      current: 'es',
    },
  icons: {
    iconfont: 'mdi',
    values: {
      town: {
        component: TownIcon
      },
      bogota: {
        component: Bogota
      },
      es: {
        component: Es
      },
      en: {
        component: En
      },
      flip_horizontal: {
        component: FlipHorizontal
      },
      flip_vertical: {
        component: FlipVertical
      },
    }
  },
});
