export default {
  light: {
    primary: '#00bcd4',
    secondary: '#03a9f4',
    tertiary: '#495057',
    accent: '#82B1FF',
    error: '#f44336',
    info: '#00bcd4',
    success: '#4caf50',
    warning: '#ff9800'
  },
  dark: {
    primary: '#00bcd4',
    secondary: '#03a9f4',
    tertiary: '#495057',
    accent: '#82B1FF',
    error: '#f44336',
    info: '#00bcd4',
    success: '#4caf50',
    warning: '#ff9800'
  }
}
