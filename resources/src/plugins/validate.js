import Vue from 'vue'
import VeeValidate, { Validator } from "vee-validate";
import es from "vee-validate/dist/locale/es";
import en from "vee-validate/dist/locale/en";
import {Access} from "@/models/services/Security/Access";
Validator.localize("es", es);

const isUnique = (value, [id]) => {
  return (new Access({ username: value, sim_id: id })).check()
              .then( ( response ) => {
                return response
              })
              .catch((error) => {
                return error;
              })
}

Validator.extend('unique', {
  validate: isUnique,
  getMessage: (field, params, data) => data.message
}, {
  immediate: false
});

const attributes = {
  username: 'usuario',
  password: 'contraseña',
  name: 'nombre',
  description: 'descripción',
  available_from: 'disponible desde',
  available_until: 'disponible hasta',
  profile: 'perfil',
  profile_pic: 'foto de perfil',
  age: 'edad',
  campaign: 'campaña',
  campaign_id: 'campaña',
  candidates: 'candidatos',
  avatar: 'avatar',
  question: 'pregunta',
  security_answer: 'respuesta',
  document: 'documento',
  security_question_id: 'pregunta',
};

Vue.use(VeeValidate, {
  locale: "es",
  dictionary: {
    es: {
      messages: es,
      attributes: attributes
    },
    en
  }
});

/*
  this.errors[key][0]
  fieldsBagName: "veeFields",
  locale: "es"
 */
