import Vue from "vue"
import axios from 'axios';
import VueAxios from 'vue-axios';

Vue.use(VueAxios, axios);
Vue.axios.defaults.baseURL = process.env.NODE_ENV === 'development' ? process.env.VUE_APP_API_URL_DEV : process.env.VUE_APP_API_URL_PROD;
Vue.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
Vue.axios.defaults.headers.common["Accept"] = "application/json";
Vue.axios.defaults.headers.common["Content-Type"] = "application/json";

window.CancelToken = Vue.axios.CancelToken;
window.source = null;
