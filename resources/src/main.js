// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

// Components
import './components'

// Plugins
import './plugins'

// Sync router with store
import { sync } from 'vuex-router-sync'

import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'

import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import i18n from './i18n'
import vuetify from '@/plugins/vuetify'

// Sync store with router
sync(store, router);

//Mixins
import VuetifyKit from "./mixins";
Vue.use( VuetifyKit );

Vue.config.productionTip = false

export const vm = new Vue({
  vuetify,
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')

/*
router.beforeResolve((to, from, next) => {
  // If this isn't an initial page load.
  if (to.name) {
    // Start the route progress bar.
    vm.loadingPage = true
  }
  next()
});

router.afterEach((to, from) => {
  // Complete the animation of the route progress bar.
  setTimeout(() => app.loadingPage = false, 1500)
});
 */