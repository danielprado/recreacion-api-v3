import AppLocaleMixin from "./locale";
import BodyClassMixins from "./body-class";
import FeaturesMixin from "./features";
import AxiosInterceptorMixin from "./axios";
import ResponsiveAndOrientationMixin from "./responsive";
import SnackMixin from "./snack";
import GlobalSelects from "@/mixins/selects";
import MomentMixin from "@/mixins/moment";

export default {
    install(Vue) {
        Vue.use( AppLocaleMixin );
        Vue.use( AxiosInterceptorMixin );
        Vue.use( BodyClassMixins );
        Vue.use( FeaturesMixin );
        Vue.use( ResponsiveAndOrientationMixin );
        Vue.use( SnackMixin );
        Vue.use( GlobalSelects );
        Vue.use( MomentMixin );
    }
}
