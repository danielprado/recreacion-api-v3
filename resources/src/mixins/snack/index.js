import {mapMutations} from "vuex";
/**
 * Mixin to show/hide global snackbar
 */


const SnackMixin = {
    install(Vue) {
        Vue.mixin({
            methods: {
                ...mapMutations('app', ['setSnackType', 'setSnackMessage', 'setSnack']),
                notifyOnUpdate() {
                    this.setSnackType( 'info' );
                    let message = this.$t ? this.$t('Core.NewVersion') : 'New Update Available!'
                    this.setSnackMessage( message );
                    this.setSnack(true);
                },
                notifyOnError( message = null ) {
                    this.setSnackType( 'error' );
                    this.setSnackMessage( message || this.$t('Core.UnexpectedError') );
                    this.setSnack(true);
                },
                notifyOnSuccess(message) {
                    this.setSnackType( 'success' );
                    this.setSnackMessage( message );
                    this.setSnack(true);
                },
                onCloseNotification: function () {
                    this.setSnackType( 'black' );
                    this.setSnackMessage( '' );
                    this.setSnack(!this.$store.state.app.snack);
                }
            }
        });
    }
};

export default SnackMixin;
