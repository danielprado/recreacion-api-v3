/**
 * Set selects attributes
 */
import {Country} from "@/models/services/Security/Country";
import {Program} from "@/models/services/Scheme/Program";
import {Activity} from "@/models/services/Scheme/Activity";
import {SubActivity} from "@/models/services/Scheme/SubActivity";
import {Thematic} from "@/models/services/Scheme/Thematic";
import {Component} from "@/models/services/Scheme/Component";
import {Location} from "@/models/services/Scheme/Location";
import {Upz} from "@/models/services/Scheme/Upz";
import {Neighborhood} from "@/models/services/Scheme/Neighborhood";
import {PopulationCharacteristic} from "@/models/services/Scheme/PopulationCharacteristic";
import {SpecificPopulationCharacteristic} from "@/models/services/Scheme/SpecificPopulationCharacteristic";

const GlobalSelects = {
  install(Vue) {
    Vue.mixin({
      data: () => ({
        document_types: [],
        genders: [],
        ethnicities: [],
        countries: [],
        cities: [],
        programs: [],
        activities: [],
        sub_activities: [],
        thematics: [],
        components: [],
        strategies: [],
        
        locations: [],
        locations_meta: {},
        upzs: [],
        upzs_meta: {},
        neighborhoods: [],
        neighborhoods_meta: {},
  
        population_chars: [],
        population_chars_meta: {},
        specific_population_chars: [],
        specific_population_chars_meta: {},
      }),
      methods: {
        getDocumentTypes: function () {
          this.document_types = this.$store.getters['selects/getDocumentTypes'];
          if ( this.document_types.length < 1 ) {
            this.setLoading( true );
            this.$store.dispatch('selects/document_types')
              .then(response => this.document_types = response)
              .catch(error => this.notifyOnError(error.message))
              .finally(() => this.setLoading( false ))
          }
        },
        getGenders: function () {
          this.genders = this.$store.getters['selects/getGenders'];
          if ( this.genders.length < 1 ) {
            this.setLoading( true );
            this.$store.dispatch('selects/genders')
              .then(response => this.genders = response)
              .catch(error => this.notifyOnError(error.message))
              .finally(() => this.setLoading( false ))
          }
        },
        getEthnicities: function  () {
          this.ethnicities = this.$store.getters['selects/getEthnicities'];
          if ( this.ethnicities.length < 1 ) {
            this.setLoading( true );
            this.$store.dispatch('selects/ethnicities')
              .then(response => this.ethnicities = response)
              .catch(error => this.notifyOnError(error.message))
              .finally(() => this.setLoading( false ))
          }
        },
        getCountries: function () {
          this.countries = this.$store.getters['selects/getCountries'];
          if ( this.countries.length < 1 ) {
            this.setLoading( true );
            this.$store.dispatch('selects/countries')
              .then(response => this.countries = response)
              .catch(error => this.notifyOnError(error.message))
              .finally(() => this.setLoading( false ))
          }
        },
        getCities( id = null ) {
          if ( id ) {
            this.setLoading( true );
            (new Country()).cities( id )
              .then((response) => this.cities = response.data)
              .catch(error => this.notifyOnError(error.message))
              .finally(() => this.setLoading( false ))
          } else {
            this.cities = []
          }
        },

        getPrograms: function () {
          this.programs = this.$store.getters['selects/getPrograms'];
          if ( this.programs.length < 1 ) {
            this.setLoading( true );
            this.$store.dispatch('selects/programs')
              .then(response => this.programs = response)
              .then(() => this.activities = [])
              .then(() => this.sub_activities = [])
              .then(() => this.thematics = [])
              .then(() => this.components = [])
              .then(() => this.strategies = [])
              .catch(error => this.notifyOnError(error.message))
              .finally(() => this.setLoading( false ))
          }
        },
        getActivities: function (id = null) {
          if ( id ) {
            this.setLoading( true );
            (new Program()).activities(id)
              .then((response) => this.activities = response.data)
              .then(() => this.sub_activities = [])
              .then(() => this.thematics = [])
              .then(() => this.components = [])
              .then(() => this.strategies = [])
              .catch(error => this.notifyOnError(error.message))
              .finally(() => this.setLoading( false ))
          } else {
            this.activities = [];
            this.sub_activities = []
            this.thematics = []
            this.components = []
            this.strategies = []
          }
        },
        getSubActivities: function (id = null) {
          if ( id ) {
            this.setLoading( true );
            (new Activity()).sub_activities(id)
              .then((response) => this.sub_activities = response.data)
              .then(() => this.thematics = [])
              .then(() => this.components = [])
              .then(() => this.strategies = [])
              .catch(error => this.notifyOnError(error.message))
              .finally(() => this.setLoading( false ))
          } else {
            this.sub_activities = []
            this.thematics = []
            this.components = []
            this.strategies = []
          }
        },
        getThematics: function (id = null) {
          if ( id ) {
            this.setLoading( true );
            (new SubActivity()).thematics(id)
              .then((response) => this.thematics = response.data)
              .then(() => this.components = [])
              .then(() => this.strategies = [])
              .catch(error => this.notifyOnError(error.message))
              .finally(() => this.setLoading( false ))
          } else {
            this.thematics = []
            this.components = []
            this.strategies = []
          }
        },
        getComponents: function (id = null) {
          if ( id ) {
            this.setLoading( true );
            (new Thematic()).components(id)
              .then((response) => this.components = response.data)
              .then(() => this.strategies = [])
              .catch(error => this.notifyOnError(error.message))
              .finally(() => this.setLoading( false ))
          } else {
            this.components = []
            this.strategies = []
          }
        },
        getStrategies: function (id = null) {
          if ( id ) {
            this.setLoading( true );
            (new Component()).strategies(id)
              .then((response) => this.strategies = response.data)
              .catch(error => this.notifyOnError(error.message))
              .finally(() => this.setLoading( false ))
          } else {
            this.strategies = []
          }
        },

        clearDocumentTypes: function() {
          this.$store.dispatch('selects/clear_document_types');
          this.document_types = [];
          this.getDocumentTypes();
        },
        clearGenders: function () {
          this.$store.dispatch('selects/clear_genders');
          this.genders = [];
          this.getGenders();
        },
        clearEthnicities: function() {
          this.$store.dispatch('selects/clear_ethnicities');
          this.ethnicities = [];
          this.getEthnicities();
        },
        clearCountries: function () {
          this.$store.dispatch('selects/clear_countries');
          this.countries = [];
          this.getCountries();
        },
        clearPrograms: function () {
          this.$store.dispatch('selects/clear_programs');
          this.programs = [];
          this.getPrograms();
        },
        
        getLocations: function ( options = { params: { order: [ true ], per_page: 30 } } ) {
          this.locations = [];
          this.locations_meta = {};
          this.setLoading(true);
          (new Location()).index( options )
            .then((response) => {
              this.locations.push( ...response.data );
              this.locations_meta = response.meta;
            })
            .then(() => {
              this.upzs = [];
              this.upzs_meta = {};
              this.neighborhoods = [];
              this.neighborhoods_meta = {};
            })
            .catch(error => this.notifyOnError(error.message))
            .finally(() => this.setLoading( false ))
        },
        getUpz: function (id = null, options = { params: { order: [ true ], per_page: 30 } }) {
          if ( id ) {
            (new Location()).upz(id, options)
              .then((response) => this.upzs = response.data)
              .then(() => {
                this.neighborhoods = [];
                this.neighborhoods_meta = {};
              })
              .catch(error => this.notifyOnError(error.message))
              .finally(() => this.setLoading( false ))
          } else {
            this.upzs = [];
            (new Upz()).index()
              .then((response) => {
                this.upzs.push( ...response.data );
                this.upzs_meta = response.meta;
              })
              .then(() => {
                this.neighborhoods = [];
                this.neighborhoods_meta = {};
              })
              .catch(error => this.notifyOnError(error.message))
              .finally(() => this.setLoading( false ))
          }
        },
        getNeighborhoods: function (id = null, options = { params: { order: [ true ], per_page: 30 } }) {
          if ( id ) {
            (new Upz()).neighborhoods(id, options)
              .then((response) => this.neighborhoods = response.data)
              .catch(error => this.notifyOnError(error.message))
              .finally(() => this.setLoading( false ))
          } else {
            this.upzs = [];
            (new Neighborhood()).index()
              .then((response) => {
                this.neighborhoods.push( ...response.data );
                this.neighborhoods_meta = response.meta;
              })
              .catch(error => this.notifyOnError(error.message))
              .finally(() => this.setLoading( false ))
          }
        },
        
        getPopulation: function () {
          this.population_chars = [];
          this.population_chars_meta = {};
          this.setLoading(true);
          (new PopulationCharacteristic()).index()
            .then((response) => {
              this.population_chars.push( ...response.data );
              this.population_chars_meta = response.meta;
            })
            .then(() => {
              this.specific_population_chars = [];
              this.specific_population_chars_meta = {};
            })
            .catch(error => this.notifyOnError(error.message))
            .finally(() => this.setLoading( false ))
        },
        getSpecificPopulation: function (id = {}, options = { params: { order: [ true ], per_page: 30 } }) {
          if ( id ) {
            (new PopulationCharacteristic()).specifics(id, options)
              .then((response) => this.specific_population_chars = response.data)
              .catch(error => this.notifyOnError(error.message))
              .finally(() => this.setLoading( false ))
          } else {
            this.specific_population_chars = [];
            (new SpecificPopulationCharacteristic()).index()
              .then((response) => {
                this.specific_population_chars.push( ...response.data );
                this.specific_population_chars_meta = response.meta;
              })
              .catch(error => this.notifyOnError(error.message))
              .finally(() => this.setLoading( false ))
          }
        }
      }
    });
  }
};

export default GlobalSelects;
