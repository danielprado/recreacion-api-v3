import {vm} from '@/main'
export class Errors {
  constructor() {
    this.errors = {};
  }

  all() {
    if (typeof this.errors !== "undefined" && this.any()) {
      return this.errors;
    }
    return null;
  }

  first(field) {
    if (typeof this.errors !== "undefined" && this.errors[field]) {
      return this.errors[field][0];
    }
  }

  get(field) {
    if (typeof this.errors !== "undefined" && this.errors[field]) {
      return this.errors[field];
    }
    return null;
  }

  record(errors) {
    this.errors = errors;
    if ( typeof errors === "object" && !errors.hasOwnProperty('message') ) {
      // only allow this function to be run if the validator exists
      if(!vm.hasOwnProperty('$validator')) {
        return;
      }
      // clear errors
      vm.$validator.errors.clear();

      for(let key in errors ) {
        this.addErrorToChildComponents( vm, key,  this.errors[key][0])
      }
    }
  }

  addErrorToChildComponents(that, field, errorString) {
    if(that && that.$validator && that.$validator.errors){
      const input_field = that.$validator.fields.find({ name: field });
      if(input_field){
        that.$validator.errors.add({
          field,
          msg: errorString
        });
        return;
      }
    }
    if(that.$children){
      that.$children.map(async ($cvm) => {
        this.addErrorToChildComponents($cvm,field,errorString);
      });
    }
    return;
  }

  has(field) {
    return this.errors ? this.errors.hasOwnProperty(field) : false;
  }

  any() {
    return Object.keys(this.errors).length > 0;
  }

  clear(field) {
    if (field) {
      delete this.errors[field];
      return;
    }
    this.errors = {};
  }

}
