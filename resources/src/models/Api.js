export const Api = {
  END_POINTS: {
    //Project
    REUNION_TYPES: () => `/api/reunion-types`,
    REQUEST_TYPES: () => `/api/request-types`,
    PROCESSES: () => `/api/processes`,
    SUMMONERS: () => `/api/summoners`,
    PROGRAMMING: () => `/api/programmings`,
    SCHEDULES: () => `/api/citizen-attention-schedules`,
    CITIZEN: () => `/api/citizen-attentions`,
    PARKS: () => `/api/parks`,
    PROFESSIONALS: () => `/api/professionals`,
    THEMES: () => `/api/themes`,
    CONDITIONS: () => `/api/conditions`,
    SITUATIONS: () => `/api/situations`,
    ENTITIES: () => `/api/entities`,
    FILE_TYPES: () => `/api/file-types`,
    TYPES_OF_POPULATIONS: () => `/api/types-of-populations`,
    EXECUTIONS: (id)  => `/api/programmings/${id}/executions`,
    PROGRAMMINGS_FILES: (id)  => `/api/programmings/${id}/files`,
    PROGRAMMINGS_IMAGE: (id)  => `/api/programmings/${id}/images`,
    PROGRAMMINGS_COMMITMENTS: (id) => `/api/programmings/${id}/commitments`,
    PROGRAMMINGS_COMMITMENTS_TRACINGS: (programming_id, commitment_id) => `/api/programmings/${programming_id}/commitments/${commitment_id}/tracings`,
    COMMITMENTS_CALENDAR: () => `/api/commitments/calendar`,
    GENERAL_REPORT: () => `/api/programmings/general-report`,
    ATTENTION_REPORT: () => `/api/attendance/general-report`,
    
    
    PROGRAMMED_PARKS: () => `/api/programmed-parks`,
    PROGRAMMING_COUNT: () => `/api/counters`,
    STATS: () => `/api/stats`,
    
    //Security
    USERS: () => `/api/users`,
    PROFILE: () => `/api/profile`,
    ACCESS: () => `/api/access`,
    COUNTRIES: () => `/api/countries`,
    GENDERS: () => `/api/genders`,
    ETHNICITIES: () => `/api/ethnicities`,
    DOCUMENT_TYPES: () => `/api/document-types`,
    ROLES: () => `/api/roles`,
    ROLE_PERMISSIONS: (id) => `/api/roles/${id}/permissions`,
    PERMISSIONS: () => `/api/permissions`,
    USERS_ROLES: (id) => `/api/users/${id}/roles`,

    //Schemes
    PROGRAMS: () => `/api/programs`,
    ACTIVITIES: () => `/api/activities`,
    SUB_ACTIVITIES: () => `/api/sub-activities`,
    THEMATICS: () => `/api/thematics`,
    COMPONENTS: () => `/api/components`,
    STRATEGIES: () => `/api/strategies`,
    POPULATION_CHARACTERISTICS: () => `/api/population-characteristics`,
    SPECIFICS_POPULATION_CHARACTERISTICS: () => `/api/specific-population-characteristics`,
    
    //Location
    LOCATION: () => `/api/locations`,
    UPZ: () => `/api/upz`,
    NEIGHBORHOODS: () => `/api/neighborhoods`,

    // Auth Routes
    GET_PROFILE: () => `/api/user`,
    LOGIN: () => `/oauth/token`,
    LOGOUT: () => `/logout`,
    LOCK: () => `/lock`,
    UNLOCK: () => `/unlock`
  }
};
