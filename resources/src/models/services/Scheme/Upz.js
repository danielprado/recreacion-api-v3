import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Upz extends Model {
  constructor(data = {
    name: null,
    upz_code: null,
    location_id: null,
  }) {
    super(Api.END_POINTS.UPZ(), data);
  }
  
  neighborhoods(id, options = {}) {
    return this.get( `${this.url}/${id}/neighborhoods`, options )
  }
}
