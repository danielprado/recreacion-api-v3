import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Thematic extends Model {
  constructor(data = {
    name: null,
    sub_activity_id: null,
    activity_id: null,
    program_id: null,
  }) {
    super(Api.END_POINTS.THEMATICS(), data);
  }
  components(id, options = null) {
    return this.get(`${this.url}/${id}/components`, options)
  }
}
