import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class PopulationCharacteristic extends Model {
  constructor(data = {
    name: null,
  }) {
    super(Api.END_POINTS.POPULATION_CHARACTERISTICS(), data);
  }
  
  specifics(id, options = {}) {
    return this.get( `${this.url}/${id}/specifics`, options );
  }
}
