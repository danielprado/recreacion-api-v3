import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class SpecificPopulationCharacteristic extends Model {
  constructor(data = {
    name: null,
    population_characteristic_id: null
  }) {
    super(Api.END_POINTS.SPECIFICS_POPULATION_CHARACTERISTICS(), data);
  }
}
