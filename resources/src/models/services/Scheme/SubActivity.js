import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class SubActivity extends Model {
  constructor(data = {
    name: null,
    activity_id: null,
    program_id: null,
  }) {
    super(Api.END_POINTS.SUB_ACTIVITIES(), data);
  }
  thematics(id, options = null) {
    return this.get(`${this.url}/${id}/thematics`, options)
  }
}
