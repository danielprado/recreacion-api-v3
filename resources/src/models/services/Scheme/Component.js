import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Component extends Model {
  constructor(data = {
    name: null,
    thematic_id: null,
    sub_activity_id: null,
    activity_id: null,
    program_id: null,
  }) {
    super(Api.END_POINTS.COMPONENTS(), data);
  }
  strategies(id, options = null) {
    return this.get(`${this.url}/${id}/strategies`, options)
  }
}
