import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Activity extends Model {
  constructor(data = {
    name: null,
    program_id: null
  }) {
    super(Api.END_POINTS.ACTIVITIES(), data);
  }
  sub_activities(id, options = null) {
    return this.get(`${this.url}/${id}/sub-activities`, options)
  }
}