import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Program extends Model {
  constructor(data = {
    name: null,
  }) {
    super(Api.END_POINTS.PROGRAMS(), data);
  }

  all(options = {}) {
    return this.get(`${this.url}/all`, options)
  }
  
  activities(id, options = null) {
    return this.get(`${this.url}/${id}/activities`, options)
  }
}
