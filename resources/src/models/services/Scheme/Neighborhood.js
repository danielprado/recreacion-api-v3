import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Neighborhood extends Model {
  constructor(data = {
    name: null,
    upz_id: null,
    location_id: null,
  }) {
    super(Api.END_POINTS.NEIGHBORHOODS(), data);
  }
}
