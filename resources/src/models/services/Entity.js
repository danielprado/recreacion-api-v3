import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Entity extends Model {
  constructor() {
    super(Api.END_POINTS.ENTITIES(), {});
  }

}