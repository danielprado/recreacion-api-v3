import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class CitizenAttentionSchedule extends Model {
  constructor() {
    super(Api.END_POINTS.SCHEDULES(), {});
  }
}