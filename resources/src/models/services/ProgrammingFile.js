import {Model} from "@/models/Model";
import {Api} from "@/models/Api";
import {vm} from "@/main";

export class ProgrammingFile extends Model {
  constructor(id, data = {
    file_type_id: null,
    file: null,
  }) {
    super(Api.END_POINTS.PROGRAMMINGS_FILES(id), data);
  }

  storeWithFile( ) {
    return new Promise( (resolve, reject) => {
      let config = {
        header : {
          'Content-Type' : 'multipart/form-data'
        }
      };
      let form_data = new FormData();
      form_data.append( 'file', this.file);
      form_data.append( 'file_type_id', this.file_type_id );

      vm.axios.post( `${this.url}`, form_data, config )
        .then((response) => resolve( response.data ))
        .then(() => this.reset() )
        .then(() => vm.$validator.reset())
        .catch((error) => {
          if ( error.response && error.response.data ) {
            reject( error.response.data );
          } else {
            reject( error );
          }
        })

    })
  }
}