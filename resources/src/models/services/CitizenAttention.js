import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class CitizenAttention extends Model {
  constructor(data = {
    date: null,
    working_day: null,
    who_cancel: null,
    reason: null,
    reprogramming_reason: null
  }) {
    super(Api.END_POINTS.CITIZEN(), data);
  }

  calendar( options = {} ) {
    return this.get( `${this.url}/calendar`, options );
  }
  
  cancel( id, options = {} ) {
    return this.post( `${this.url}/${id}/cancel`, options );
  }
}