import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class ProgrammingDetails extends Model {
  constructor(id, data = {
    activities: [],
    themes: [],
    other: null,
  }) {
    super(`${Api.END_POINTS.PROGRAMMING()}/${id}/details`, data);
  }

}