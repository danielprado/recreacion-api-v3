import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class RequestType extends Model {
  constructor() {
    super(Api.END_POINTS.REQUEST_TYPES(), {});
  }

}