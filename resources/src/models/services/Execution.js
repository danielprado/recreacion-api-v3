import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Execution extends Model {
  constructor(id, data = {
    entity_id: null,
    type_of_population_id: null,
    condition_id: null,
    situation_id: null,
    f_0_5: 0,
    m_0_5: 0,
    f_6_12: 0,
    m_6_12: 0,
    f_13_17: 0,
    m_13_17: 0,
    f_18_26: 0,
    m_18_26: 0,
    f_27_59: 0,
    m_27_59: 0,
    f_60_more: 0,
    m_60_more: 0,
    observation: null,
  }) {
    super(Api.END_POINTS.EXECUTIONS(id), data);
  }
}