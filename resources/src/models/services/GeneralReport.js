import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class GeneralReport extends Model {
  constructor(data = {
    start_day: null,
    end_day: null,
    park_id: null,
    who_summons: [],
    process_id: [],
    request_type_id: [],
    entity_id: [],
    type_of_population_id: [],
    condition_id: [],
    situation_id: [],
    professionals: []
  }) {
    super(Api.END_POINTS.GENERAL_REPORT(), data);
  }
}