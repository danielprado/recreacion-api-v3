import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Activity extends Model {
  constructor(id) {
    super(Api.END_POINTS.ACTIVITIES(id), {});
  }
}