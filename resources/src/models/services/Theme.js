import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Theme extends Model {
  constructor() {
    super(Api.END_POINTS.THEMES(), {});
  }
}