import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Counters extends Model {
  constructor() {
    super('', {});
  }
  
  parks(options = {}) {
    return this.get( Api.END_POINTS.PROGRAMMED_PARKS(), options )
  }
  
  programmings(options = {}) {
    return this.get( Api.END_POINTS.PROGRAMMING_COUNT(), options )
  }
  
  stats(options = {}) {
    return this.get( Api.END_POINTS.STATS(), options )
  }
  
}