import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Process extends Model {
  constructor() {
    super(Api.END_POINTS.PROCESSES(), {});
  }
}