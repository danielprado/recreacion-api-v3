import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Permission extends Model {
  constructor(data = {
    name: null,
    display_name: null,
    description: null
  }) {
    super(Api.END_POINTS.PERMISSIONS(), data);
  }
  all(options = {}) {
    return this.get(`${this.url}/all`, options)
  }
}