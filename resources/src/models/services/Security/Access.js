import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Access extends Model {
  constructor(data = {
    sim_id: null,
    username: null,
    password: null,
  }) {
    super(Api.END_POINTS.ACCESS(), data);
  }

  check (options = {}) {
    return this.post( `${this.url}/check`, options )
  }
}