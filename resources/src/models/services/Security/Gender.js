import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Gender extends Model {
  constructor(data = {}) {
    super(Api.END_POINTS.GENDERS(), data);
  }
}