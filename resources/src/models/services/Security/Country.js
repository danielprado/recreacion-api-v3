import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Country extends Model {
  constructor(data = {}) {
    super(Api.END_POINTS.COUNTRIES(), data);
  }

  cities(id, options = {}) {
    return this.get( `${this.url}/${id}/cities`, options )
  }
}