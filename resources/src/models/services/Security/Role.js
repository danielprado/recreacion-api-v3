import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Role extends Model {
  constructor(data = {
    name: null,
    display_name: null,
    description: null
  }) {
    super(Api.END_POINTS.ROLES(), data);
  }

  all(options = {}) {
    return this.get(`${this.url}/all`, options)
  }
}