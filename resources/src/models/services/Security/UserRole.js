import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class UserRole extends Model {
  constructor(id = null, data = {
    user: null,
    roles: []
  }) {
    super(Api.END_POINTS.USERS_ROLES(id), data);
  }

  assigned(id, options = {}) {
    return this.get( `/api/users/${id}/assigned-roles`, options );
  }
}