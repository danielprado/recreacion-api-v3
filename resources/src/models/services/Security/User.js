import { Model } from "@/models/Model";
import { Api } from "@/models/Api";

export class User extends Model {
  constructor(data = {
    name: null,
    email: null,
    contract: null,
    contract_initial_date: null,
    contract_final_date: null,
    virtual_record: null,
    username: null,
    password: null,
    password_confirmation: null,
    sim_id: null,
  }) {
    super(Api.END_POINTS.USERS(), data);
  }
  
  profile(options = {}) {
      return this.get(Api.END_POINTS.GET_PROFILE(), options);
  };

  login() {
    return this.post(Api.END_POINTS.LOGIN(), this.data());
  }

  logout() {
    return this.post( Api.END_POINTS.LOGOUT() );
  }
  
  lock() {
    return this.post(Api.END_POINTS.LOCK());
  }
  
  unlock() {
    return this.post( Api.END_POINTS.UNLOCK(), this.data() );
  }
}
