import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Profile extends Model {
  constructor(data = {
    document_type_id: null,
    document: null,
    lastname: null,
    second_lastname: null,
    first_name: null,
    second_name: null,
    birthday: null,
    gender_id: null,
    ethnicity_id: null,
    country_id: null,
    city_id: null
  }) {
    super(Api.END_POINTS.PROFILE(), data);
  }

  check(options = {}) {
    return this.post( `${this.url}/check`, options )
  }
}