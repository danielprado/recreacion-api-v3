import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class RolePermission extends Model {
  constructor(role = null, data = {
    permissions: []
  }) {
    super(Api.END_POINTS.ROLE_PERMISSIONS(role), data);
  }
}