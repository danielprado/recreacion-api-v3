import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Ethnicity extends Model {
  constructor(data = {}) {
    super(Api.END_POINTS.ETHNICITIES(), data);
  }
}