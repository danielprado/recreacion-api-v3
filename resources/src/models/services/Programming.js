import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Programming extends Model {
  constructor(data = {
    date: null,
    initial_hour: null,
    final_hour: null,
    objective: null,
    reunion_type_id: null,
    park_id: null,
    park: null,
    upz_id: null,
    upz: null,
    place: null,
    process_id: null,
    who_summons: null,
    which: null,
    request_type_id: null,
    who_cancel: null,
    reason: null,
  }) {
    super(Api.END_POINTS.PROGRAMMING(), data);
  }
  
  calendar( options = {} ) {
    return this.get( `${this.url}/calendar`, options );
  }
  
  cancel( id, options = {} ) {
    return this.post( `${this.url}/${id}/cancel`, options );
  }
  
  excel( id ) {
    let base = process.env.NODE_ENV === 'development' ? process.env.VUE_APP_API_URL_DEV : process.env.VUE_APP_API_URL_PROD;
    return `${base}${this.url}/${id}/excel`
  }
}