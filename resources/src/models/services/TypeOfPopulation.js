import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class TypeOfPopulation extends Model {
  constructor() {
    super(Api.END_POINTS.TYPES_OF_POPULATIONS(), {});
  }
}