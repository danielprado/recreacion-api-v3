import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class CommitmentCalendar extends Model {
  constructor() {
    super(Api.END_POINTS.COMMITMENTS_CALENDAR(), {});
  }
}