import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Commitment extends Model {
  constructor(id, data = {
    responsable: null,
    description: null,
    date: null
  }) {
    super(Api.END_POINTS.PROGRAMMINGS_COMMITMENTS(id), data);
  }
}