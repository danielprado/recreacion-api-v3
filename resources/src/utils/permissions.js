export const permissions = {
  security: [
    'create-users',
    'update-users',
    'delete-users',
    'view-users',
    'create-roles',
    'update-roles',
    'delete-roles',
    'view-roles',
    'create-permissions',
    'update-permissions',
    'delete-permissions',
    'view-permissions',
    'assign-permissions',
    'assign-roles',
    'create-programs',
    'update-programs',
    'delete-programs',
    'view-programs',
    'create-activities',
    'update-activities',
    'delete-activities',
    'view-activities',
    'create-sub-activities',
    'update-sub-activities',
    'delete-sub-activities',
    'view-sub-activities',
    'create-thematics',
    'update-thematics',
    'delete-thematics',
    'view-thematics',
    'create-components',
    'update-components',
    'delete-components',
    'view-components',
    'create-strategies',
    'update-strategies',
    'delete-strategies',
    'view-strategies',
  ],
  users: [
    'create-users',
    'update-users',
    'delete-users',
    'view-users',
  ],
  roles: [
    'create-roles',
    'update-roles',
    'delete-roles',
    'view-roles',
  ],
  permissions: [
    'create-permissions',
    'update-permissions',
    'delete-permissions',
    'view-permissions',
  ],
  assign_permissions: [
    'assign-permissions',
  ],
  assign_role: [
    'assign-roles',
  ],
  schema_activities: [
    'create-programs',//0
    'update-programs',//1
    'delete-programs',//2
    'view-programs',//3
    'create-activities',//4
    'update-activities',//5
    'delete-activities',//6
    'view-activities',//7
    'create-sub-activities',//8
    'update-sub-activities',//9
    'delete-sub-activities',//10
    'view-sub-activities',//11
    'create-thematics',//12
    'update-thematics',//13
    'delete-thematics',//14
    'view-thematics',//15
    'create-components',//16
    'update-components',//17
    'delete-components',//18
    'view-components',//19
    'create-strategies',//20
    'update-strategies',//21
    'delete-strategies',//22
    'view-strategies',//23
  ],
  scheme_locations: [
    'create-locations',//0
    'update-locations',//1
    'delete-locations',//2
    'view-locations',//3
    'create-upz',//4
    'update-upz',//5
    'delete-upz',//6
    'view-upz',//7
    'create-neighborhoods',//8
    'update-neighborhoods',//9
    'delete-neighborhoods',//10
    'view-neighborhoods',//11
  ],
  scheme_population: [
    'create-population',//0
    'update-population',//1
    'delete-population',//2
    'view-population',//3
    'create-specific-population',//4
    'update-specific-population',//5
    'delete-specific-population',//6
    'view-specific-population',//7
  ]
};
















