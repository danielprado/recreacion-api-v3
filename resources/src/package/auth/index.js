import store from "@/store";
import {User} from "@/models/services/Security/User";
import {permissions} from "@/utils/permissions";

export default function(Vue) {
    Vue.auth = {
      login: ( data ) => {
        return new Promise( (resolve, reject) =>  {
          (new User({
            grant_type: process.env.VUE_APP_GRANT_TYPE,
            client_id: process.env.VUE_APP_CLIENT_ID,
            client_secret: process.env.VUE_APP_CLIENT_SECRET,
            ...data
          })).login()
            .then((response) => {
              store.dispatch('user/login', response)
                    .then(() => resolve( response ) )
            })
            .catch( error => reject( error ) )
        })
      },
      setUser: () => {
          return new Promise( (resolve, reject) =>  {
              new User().profile()
                  .then((response) => {
                      store.dispatch('user/user', response.data)
                          .then(() => resolve(response))
                  })
                  .catch(error => reject(error))
          })
      },
      logout: () => {
        return new Promise((resolve, reject) => {
          (new User({})).logout()
            .then( response => {
              store.dispatch('user/logout')
                .then(() => resolve( response ))
            })
            .catch( error => reject( error ) )
            .finally(() => store.dispatch('user/logout'))
        })
      },
      lock: () => {
        return new Promise((resolve, reject) => {
          (new User({})).lock()
            .then( response => resolve(response))
            .catch( error => {
              store.dispatch('user/logout')
                .then(() => reject( error ) );
            })
        })
      },
      unlock: (data) => {
        return new Promise((resolve, reject) => {
          (new User({
            grant_type: process.env.VUE_APP_GRANT_TYPE,
            client_id: process.env.VUE_APP_CLIENT_ID,
            client_secret: process.env.VUE_APP_CLIENT_SECRET,
            ...data
          })).unlock()
            .then( response => resolve(response))
            .catch( error => {
              store.dispatch('user/logout')
                .then(() => reject( error ) );
            })
        })
      },
      ready: () => !!store,
      isLoggedIn: () => !!store.getters['user/isLoggedIn'],
      isNotLoggedIn () { return  !this.isLoggedIn() },
      can: ( perms ) => {
        let permissions = store.getters['user/getPermissions'];
        if ( permissions && permissions.length > 0 && perms ) {
          if ( typeof perms === "string") {
            return permissions.indexOf(perms) !== -1;
          } else {
            let some = perms.map( (p) => {
              return permissions.indexOf( p ) !== -1;
            });
            return some.includes(true);
          }
        }
        return false;
      },
      permissions: permissions
    },
    Object.defineProperties(Vue.prototype, {
      $auth: {
        get() {
          return Vue.auth;
        }
      }
    });
}
