@extends('errors.illustrated-layout')

@section('title', trans('validation.handler.method_allow'))
@section('code', '405')
@section('message', trans('validation.handler.method_allow'))
@section('image')
    <div style="background-image: url({{ asset('public/svg/404.svg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center"></div>
@endsection
