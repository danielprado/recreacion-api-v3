<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('username')->unique();
            $table->string('password');
            $table->string('contract')->nullable()->comment('Contrato IDRD');
            $table->date('contract_initial_date')->nullable()->comment('Fecha inicial del contrato');
            $table->date('contract_final_date')->nullable()->comment('Fecha final del contrato');
            $table->string('virtual_record')->nullable()->comment('Expediente Virtual');
            $table->unsignedBigInteger('sim_id')->unique()->comment('Id que identifica a la persona en el SIM');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
