<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpzsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upzs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('upz_code');
            $table->unsignedBigInteger('location_id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('location_id')->references('id')
              ->on('locations')
              ->onDelete('cascade')
              ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upzs');
    }
}
