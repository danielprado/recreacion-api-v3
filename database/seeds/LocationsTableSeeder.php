<?php

use IDRD\Entities\Scheme\Location;
use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locations = [
          ['id' => 1, 'name' =>	'USAQUÉN'],
          ['id' => 2, 'name' =>	'CHAPINERO'],
          ['id' => 3, 'name' =>	'SANTA FE'],
          ['id' => 4, 'name' =>	'SAN CRISTOBAL'],
          ['id' => 5, 'name' =>	'USME'],
          ['id' => 6, 'name' =>	'TUNJUELITO'],
          ['id' => 7, 'name' =>	'BOSA'],
          ['id' => 8, 'name' =>	'KENNEDY'],
          ['id' => 9, 'name' =>	'FONTIBÓN'],
          ['id' => 10, 'name' =>	'ENGATIVÁ'],
          ['id' => 11, 'name' =>	'SUBA'],
          ['id' => 12, 'name' =>	'BARRIOS UNIDOS'],
          ['id' => 13, 'name' =>	'TEUSAQUILLO'],
          ['id' => 14, 'name' =>	'LOS MÁRTIRES'],
          ['id' => 15, 'name' =>	'ANTONIO NARIÑO'],
          ['id' => 16, 'name' =>	'PUENTE ARANDA'],
          ['id' => 17, 'name' =>	'LA CANDELARIA'],
          ['id' => 18, 'name' =>	'RAFAEL URIBE'],
          ['id' => 19, 'name' =>	'CIUDAD BOLIVAR'],
          ['id' => 20, 'name' =>	'SUMAPÁZ'],
          ['id' => 21, 'name' =>	'DISTRITAL INFANCIA'],
          ['id' => 22, 'name' =>	'DISTRITAL JUVENTUD'],
          ['id' => 23, 'name' =>	'DISTRITAL PERSONA MAYOR'],
          ['id' => 24, 'name' =>	'DISTRITAL COMUNITARIA'],
          ['id' => 25, 'name' =>	'DISTRITAL INCLUYENTE'],
        ];

        foreach ( $locations as $location ) {
          Location::create( $location );
        }
    }
}
