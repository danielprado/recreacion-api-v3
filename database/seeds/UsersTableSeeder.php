<?php

use IDRD\Entities\Security\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory( User::class, 1)->create([
            'name' => 'DANIEL ALEJANDRO PRADO MENDOZA',
            'email' => 'daniel.prado@idrd.gov.co',
            'email_verified_at' => now(),
            'username'  =>  'daniel.prado',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'virtual_record'    =>  '2019800201100150E',
            'contract'  =>  'IDRD-CTO-0070-2019',
            'contract_initial_date' =>  '2019-01-28',
            'contract_final_date'   =>  '2020-01-27',
            'sim_id'    =>  1046,
        ]);
    }
}
