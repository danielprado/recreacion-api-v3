<?php


namespace IDRD\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;

trait ApiResponse
{
    public function validation_errors($errors, $code = 422)
    {
        return response()->json($errors, $code);
    }

    /**
     * @param $message
     * @param int $code
     * @param null $details
     * @return JsonResponse
     */
    protected function error_response($message, $code = 422, $details = null )
    {
        return response()->json([
            'message' =>  $message,
            'details' => $details,
            'code'  =>  $code
        ], $code);
    }

    /**
     * @param JsonResource $collection
     * @param int $code
     * @return JsonResponse
     */
    protected function success_response(JsonResource $collection, int $code = 200 )
    {
        return $collection->response()->setStatusCode( $code );
    }

    protected function success_message($message, $code = 200, $overrideCode = null, $details = null)
    {
        return response()->json([
            'data'    =>  $message,
            'details' =>  $details,
            'code'    =>  $overrideCode ? $overrideCode : $code
        ], $code);
    }
}
