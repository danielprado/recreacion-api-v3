<?php


namespace IDRD\Http\Resources\Security;


use Illuminate\Http\Resources\Json\JsonResource;

class UserSelectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'            =>  isset( $this->id ) ? $this->id : null,
            'name'          =>  isset( $this->name ) ? $this->name : null,
        ];
    }
}