<?php

namespace IDRD\Http\Resources\Security;

use Illuminate\Http\Resources\Json\JsonResource;

class DocumentTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"            =>  isset( $this->Id_TipoDocumento ) ? $this->Id_TipoDocumento : null,
            "name"          =>  isset( $this->Nombre_TipoDocumento ) ? $this->Nombre_TipoDocumento : null,
            "description"   =>  isset( $this->Descripcion_TipoDocumento ) ? $this->Descripcion_TipoDocumento : null,
        ];
    }
}
