<?php

namespace IDRD\Http\Resources\Security;

use Illuminate\Http\Resources\Json\JsonResource;

class CityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      =>  isset( $this->Id_Ciudad ) ?     $this->Id_Ciudad : 0,
            'name'    =>  isset( $this->Nombre_Ciudad ) ? $this->Nombre_Ciudad : null,
            'country_id' =>  isset( $this->Id_Pais ) ? $this->Id_Pais : null,
        ];
    }
}
