<?php

namespace IDRD\Http\Resources\Security;

use Illuminate\Http\Resources\Json\JsonResource;

class EthnicityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"            =>  isset( $this->Id_Etnia ) ? $this->Id_Etnia : null,
            "name"          =>  isset( $this->Nombre_Etnia ) ? $this->Nombre_Etnia : null
        ];
    }
}
