<?php

namespace IDRD\Http\Resources\Security;

use Illuminate\Http\Resources\Json\JsonResource;

class CountryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  [
            'id'      =>  isset( $this->Id_Pais ) ? $this->Id_Pais : 0,
            'name'    =>  isset( $this->Nombre_Pais ) ? $this->Nombre_Pais : null,
        ];
    }
}
