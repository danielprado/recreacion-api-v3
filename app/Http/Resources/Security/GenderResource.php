<?php

namespace IDRD\Http\Resources\Security;

use Illuminate\Http\Resources\Json\JsonResource;

class GenderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"            =>  isset( $this->Id_Genero ) ? $this->Id_Genero : null,
            "name"          =>  isset( $this->Nombre_Genero ) ? $this->Nombre_Genero : null,
        ];
    }
}
