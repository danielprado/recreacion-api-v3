<?php

namespace IDRD\Http\Resources\Security;

use IDRD\Entities\Security\Permission;
use IDRD\Entities\Security\Role;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            =>  isset( $this->id ) ? $this->id : null,
            'name'          =>  isset( $this->name ) ? $this->name : null,
            'email'         =>  isset( $this->email ) ? $this->email : null,
            'username'      =>  isset( $this->username ) ? $this->username : null,
            'contract'      =>  isset( $this->contract ) ? $this->contract : null,
            'contract_initial_date' =>  isset( $this->contract_initial_date ) ? $this->contract_initial_date->format('Y-m-d') : null,
            'contract_final_date'   =>  isset( $this->contract_final_date ) ? $this->contract_final_date->format('Y-m-d') : null,
            'virtual_record'        =>  isset( $this->virtual_record ) ? $this->virtual_record : null,
            'sim_id'                =>  isset( $this->sim_id ) ? $this->sim_id : null,
            $this->merge(
                new ProfileResource( $this->profile )
            ),
            'permissions'   =>  isset( $this->roles ) ? $this->loadPermissions( $this->roles ) : [],
            'created_at' => isset( $this->created_at ) ? $this->created_at : null,
            'updated_at' => isset( $this->updated_at ) ? $this->updated_at : null,
            'deleted_at' => $this->when(isset( $this->deleted_at ) , $this->deleted_at)
        ];
    }

    public function loadPermissions( $roles )
    {
        $permissions = [];
        if ( isset($roles) ) {
            foreach( $roles as $role ) {
                $perms = Role::find( $role->id );
                if ( isset( $perms->perms ) ) {
                    foreach( $perms->perms as $perm ) {
                        if ( isset( $perm->name ) ) {
                            $permissions[] = $perm->name;
                        }
                    }
                }
            }
        }
        return $permissions;
    }
}
