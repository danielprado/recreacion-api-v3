<?php

namespace IDRD\Http\Resources\Security;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'sim'               =>  isset( $this->id ) ? $this->id : null,
            'document'          =>  isset( $this->document ) ? $this->document : '',
            'document_type_id'  =>  isset( $this->document_type_id ) ? (int) $this->document_type_id : null,
            'document_type'     =>  isset( $this->document_type ) ? $this->document_type : '',
            'document_type_description'    =>  isset( $this->document_type_description ) ? $this->document_type_description : '',
            'full_name'         => isset( $this->full_name ) ? $this->full_name : '',
            'part_name'         => isset( $this->part_name ) ? $this->part_name : '',
            'lastname'          =>  isset( $this->lastname ) ? $this->lastname : '',
            'second_lastname'   =>  isset( $this->second_lastname ) ? $this->second_lastname : '',
            'first_name'        =>  isset( $this->first_name ) ? $this->first_name : '',
            'second_name'       =>  isset( $this->second_name ) ? $this->second_name : '',
            'birthday'          =>  isset( $this->birthday ) ? $this->birthday : '',
            'country_id'        =>  isset( $this->country_id ) ? (int) $this->country_id : null,
            'country'           =>  isset( $this->country ) ? $this->country : '',
            'city'              =>  isset( $this->city_name ) ? $this->city_name : '',
            'gender_id'         =>  isset( $this->gender_id ) ? (int) $this->gender_id : null,
            'gender'            =>  isset( $this->gender ) ? $this->gender : '',
            'ethnicity_id'      =>  isset( $this->ethnicity_id ) ? (int) $this->ethnicity_id : null,
            'ethnicity'         =>  isset( $this->ethnicity ) ? $this->ethnicity : '',
            'department_id'     =>  isset( $this->department_id ) ? (int) $this->department_id : null,
            'city_id'           =>  isset( $this->city_id ) ? (int) $this->city_id : null,
            'username'          =>  isset( $this->access->Usuario ) ? $this->access->Usuario : ''
        ];
    }
}
