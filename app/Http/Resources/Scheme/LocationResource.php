<?php

namespace IDRD\Http\Resources\Scheme;

use Illuminate\Http\Resources\Json\JsonResource;

class LocationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id'          =>  isset( $this->id ) ? $this->id : null,
        'name'        =>  isset( $this->name ) ? $this->name : null,
        'created_at'  =>  isset( $this->created_at ) ? $this->created_at : null,
        'updated_at'  =>  isset( $this->updated_at ) ? $this->updated_at : null,
        'deleted_at'  =>  isset( $this->deleted_at ) ? $this->deleted_at : null,
      ];
    }
}
