<?php

namespace IDRD\Http\Resources\Scheme;

use Illuminate\Http\Resources\Json\JsonResource;

class SpecificPopulationCharacteristicResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id'          =>  isset( $this->id ) ? $this->id : null,
          'name'        =>  isset( $this->name ) ? $this->name : null,
          'population_id'  =>  isset( $this->population_id ) ? $this->population_id : null,
          'population'     =>  isset( $this->population->name ) ? $this->population->name : null,
          'created_at'  =>  isset( $this->created_at ) ? $this->created_at : null,
          'updated_at'  =>  isset( $this->updated_at ) ? $this->updated_at : null,
          'deleted_at'  =>  isset( $this->deleted_at ) ? $this->deleted_at : null,
        ];
    }
}
