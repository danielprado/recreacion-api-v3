<?php

namespace IDRD\Http\Resources\Scheme;

use Illuminate\Http\Resources\Json\JsonResource;

class UpzResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id'          =>  isset( $this->id ) ? $this->id : null,
          'name'        =>  isset( $this->name ) ? $this->name : null,
          'upz_code'    =>  isset( $this->upz_code ) ? $this->upz_code : null,
          'location_id' =>  isset( $this->location_id ) ? $this->location_id : null,
          'location'    =>  isset( $this->location->name ) ? $this->location->name : null,
          'created_at'  =>  isset( $this->created_at ) ? $this->created_at : null,
          'updated_at'  =>  isset( $this->updated_at ) ? $this->updated_at : null,
          'deleted_at'  =>  isset( $this->deleted_at ) ? $this->deleted_at : null,
        ];
    }
}
