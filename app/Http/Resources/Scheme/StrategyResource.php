<?php

namespace IDRD\Http\Resources\Scheme;

use Illuminate\Http\Resources\Json\JsonResource;

class StrategyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id'          =>  isset( $this->id ) ? $this->id : null,
          'name'        =>  isset( $this->name ) ? $this->name : null,
          'component_id'=>  isset( $this->component_id ) ? $this->component_id : null,
          'component'   =>  isset( $this->component->name ) ? $this->component->name : null,
          'thematic_id' =>  isset( $this->component->thematic_id ) ? $this->component->thematic_id : null,
          'thematic'    =>  isset( $this->component->thematic->name ) ? $this->component->thematic->name : null,
          'activity_id' =>  isset( $this->component->thematic->sub_activity->activity_id ) ? $this->component->thematic->sub_activity->activity_id : null,
          'activity'    =>  isset( $this->component->thematic->sub_activity->activity->name ) ? $this->component->thematic->sub_activity->activity->name : null,
          'sub_activity_id' =>  isset( $this->component->thematic->sub_activity_id ) ? $this->component->thematic->sub_activity_id : null,
          'sub_activity'    =>  isset( $this->component->thematic->sub_activity->name ) ? $this->component->thematic->sub_activity->name : null,
          'program_id'  =>  isset( $this->component->thematic->sub_activity->activity->program->id ) ? $this->component->thematic->sub_activity->activity->program->id : null,
          'program'     =>  isset( $this->component->thematic->sub_activity->activity->program->name ) ? $this->component->thematic->sub_activity->activity->program->name : null,
          'created_at'  =>  isset( $this->created_at ) ? $this->created_at : null,
          'updated_at'  =>  isset( $this->updated_at ) ? $this->updated_at : null,
          'deleted_at'  =>  isset( $this->deleted_at ) ? $this->deleted_at : null,
        ];
    }
}
