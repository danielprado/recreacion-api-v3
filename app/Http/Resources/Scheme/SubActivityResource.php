<?php

namespace IDRD\Http\Resources\Scheme;

use Illuminate\Http\Resources\Json\JsonResource;

class SubActivityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  [
          'id'          =>  isset( $this->id ) ? $this->id : null,
          'name'        =>  isset( $this->name ) ? $this->name : null,
          'activity_id' =>  isset( $this->activity_id ) ? $this->activity_id : null,
          'activity'    =>  isset( $this->activity->name ) ? $this->activity->name : null,
          'program_id'  =>  isset( $this->activity->program->id ) ? $this->activity->program->id : null,
          'program'     =>  isset( $this->activity->program->name ) ? $this->activity->program->name : null,
          'created_at'  =>  isset( $this->created_at ) ? $this->created_at : null,
          'updated_at'  =>  isset( $this->updated_at ) ? $this->updated_at : null,
          'deleted_at'  =>  isset( $this->deleted_at ) ? $this->deleted_at : null,
        ];
    }
}
