<?php

namespace IDRD\Http\Requests\Scheme;

use Illuminate\Foundation\Http\FormRequest;

class StoreSubActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can(['create-sub-activities']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          =>  'required|min:3|max:191',
            'activity_id'    =>  'required|numeric|exists:activities,id'
        ];
    }
}
