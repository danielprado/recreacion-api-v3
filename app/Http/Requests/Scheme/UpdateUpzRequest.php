<?php

namespace IDRD\Http\Requests\Scheme;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUpzRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can(['update-upz']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'name'        =>  'required|min:3|max:191',
        'upz_code'    =>  'required|min:3|max:191|unique:upzs,upz_code,'.$this->route('upz')->id,
        'location_id' =>  'required|numeric|exists:locations,id'
      ];
    }
}
