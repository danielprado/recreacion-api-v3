<?php

namespace IDRD\Http\Requests\Security;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePermissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can(['update-permissions']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          =>  'required|alpha_dash|min:3|max:30|unique:roles,name,'.$this->route('permission')->id,
            'display_name'  =>  'required|min:3|max:80',
            'description'   =>  'required|min:3|max:2500'
        ];
    }
}
