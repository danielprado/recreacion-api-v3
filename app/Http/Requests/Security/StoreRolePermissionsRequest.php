<?php

namespace IDRD\Http\Requests\Security;

use Illuminate\Foundation\Http\FormRequest;

class StoreRolePermissionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can(['assign-permissions']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'permissions'   =>  'required|array',
            'permissions.*' =>  'numeric|exists:permissions,id'
        ];
    }
}
