<?php

namespace IDRD\Http\Requests\Security;

use Illuminate\Foundation\Http\FormRequest;

class MassiveUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can(['create-users']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file'  =>  'required|file|mimes:.xls,.xlsx'
        ];
    }
}
