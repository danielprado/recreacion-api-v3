<?php

namespace IDRD\Http\Requests\Security;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can(['update-users']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document'              =>  'required|digits_between:3,12|unique:mysql_users.persona,Cedula,'.$this->route('person')->id.',Id_Persona',
            'document_type_id'      =>  'required|numeric|exists:mysql_users.tipo_documento,Id_TipoDocumento',
            'lastname'              =>  'required|min:3|max:80',
            'second_lastname'       =>  'nullable|min:3|max:80',
            'first_name'            =>  'required|min:3|max:80',
            'second_name'           =>  'nullable|min:3|max:80',
            'birthday'              =>  'required|date',
            'country_id'            =>  'required|numeric|exists:mysql_users.pais,Id_Pais',
            'gender_id'             =>  'required|numeric|exists:mysql_users.genero,Id_Genero',
            'ethnicity_id'          =>  'required|numeric|exists:mysql_users.etnia,Id_Etnia',
            'city_id'               =>  'required|numeric|exists:mysql_users.ciudad,Id_Ciudad'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'document.unique'           =>  __('validation.handler.document_exists_in_sim'),
            'document_type_id.exists'   =>  __('validation.handler.resource_not_found'),
            'country_id.exists'         =>  __('validation.handler.resource_not_found'),
            'gender_id.exists'          =>  __('validation.handler.resource_not_found'),
            'ethnicity_id.exists'       =>  __('validation.handler.resource_not_found'),
            'city_id.exists'            =>  __('validation.handler.resource_not_found'),
        ];
    }
}
