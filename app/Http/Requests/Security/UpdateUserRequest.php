<?php

namespace IDRD\Http\Requests\Security;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can(['update-users']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  =>  'required|min:6|max:80',
            'email'                 =>  'nullable|email|unique:users,email,'.$this->route('user')->id,
            'username'              =>  'required|min:6|max:80|exists:mysql_users.acceso,Usuario|unique:users,username,'.$this->route('user')->id,
            'password'              =>  'required|min:6|max:12',
            'contract'              =>  'nullable|min:6|max:80',
            'contract_initial_date' =>  'nullable|date|before_or_equal:contract_initial_date',
            'contract_final_date'   =>  'nullable|date|after_or_equal:contract_initial_date',
            'virtual_record'        =>  'nullable|min:6|max:80',
            'sim_id'                =>  'required|exists:mysql_users.view_users_profile,id'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'username.unique' => __('validation.handler.username_taken'),
            'username.exists' => __('validation.handler.user_does_not_exists_in_sim'),
            'sim_id.exists'   => __('validation.handler.sim_id_not_exists_in_sim'),
        ];
    }
}
