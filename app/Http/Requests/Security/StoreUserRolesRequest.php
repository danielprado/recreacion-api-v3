<?php

namespace IDRD\Http\Requests\Security;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRolesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can(['assign-roles']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'roles'   =>  'required|array',
            'roles.*' =>  'numeric|exists:roles,id'
        ];
    }
}
