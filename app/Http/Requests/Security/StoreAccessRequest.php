<?php

namespace IDRD\Http\Requests\Security;

use Illuminate\Foundation\Http\FormRequest;

class StoreAccessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can(['create-users', 'update-users']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'     =>  'required|min:6|max:80|unique:mysql_users.acceso,Usuario',
            'password'     =>  'required|min:3|max:12'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'username.unique' => __('validation.handler.username_taken')
        ];
    }
}
