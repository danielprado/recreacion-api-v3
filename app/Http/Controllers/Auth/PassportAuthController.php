<?php

namespace IDRD\Http\Controllers\Auth;

use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;

class PassportAuthController extends Controller
{
    public function logout(Request $request)
    {
      $user = auth()->user()->token();
      $user->revoke();
      return response()->json([
        'data'  =>  trans('validation.handler.logout'),
        'code'  =>  200
      ], 200);
    }
}
