<?php

namespace IDRD\Http\Controllers\Security;

use IDRD\Entities\Security\Ethnicity;
use IDRD\Http\Resources\Security\EthnicityResource;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;

class EthnicityController extends Controller
{
    public function index()
    {
        return $this->success_response(
            EthnicityResource::collection( Ethnicity::all() ),
            200
        );
    }
}
