<?php

namespace IDRD\Http\Controllers\Security;

use IDRD\Entities\Security\Permission;
use IDRD\Entities\Security\Role;
use IDRD\Http\Requests\Security\StoreRolePermissionsRequest;
use IDRD\Http\Resources\Security\PermissionResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;
use Illuminate\Http\Response;

class RolePermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Role $role
     * @return JsonResponse
     */
    public function index(Role $role)
    {
        return response()->json([
            'data'  =>  $role->perms,
            'code'  =>  200
        ],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRolePermissionsRequest $request
     * @param Role $role
     * @return JsonResponse
     */
    public function store(StoreRolePermissionsRequest $request, Role $role)
    {
        $role->perms()->sync([]);
        $role->attachPermissions( $request->get('permissions') );
        return $this->success_message(__('validation.handler.success'), 201);
    }
}
