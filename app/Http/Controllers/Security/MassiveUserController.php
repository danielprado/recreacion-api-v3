<?php

namespace IDRD\Http\Controllers\Security;

use IDRD\Http\Requests\Security\MassiveUserRequest;
use IDRD\Imports\SimImport;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class MassiveUserController extends Controller
{
    public function massive_access_sim(MassiveUserRequest $request)
    {
      $import = new SimImport();
      $import->import( $request->file('file') );

      $errors = [];
      foreach ($import->failures() as $failure) {
        $errors[] = [
          'row'       => $failure->row(), // row that went wrong
          'attribute' => $failure->attribute(), // either heading key (if using heading row concern) or column index
          'errors'    => $failure->errors(), // Actual error messages from Laravel validator
          'values'    => $failure->values(), // The values of the row that has failed.
        ];
      }

      if ( count( $errors ) > 0 ) {
        return $this->error_response( '', 422,  $errors);
      }

      return response()->json([
          'data'    =>  'Se han añadido los usarios satisfactoriamente.'
      ], 201);
    }
}
