<?php

namespace IDRD\Http\Controllers\Security;

use IDRD\Entities\Security\Profile;
use IDRD\Entities\Security\User;
use IDRD\Http\Requests\Security\StoreUserRolesRequest;
use IDRD\Http\Resources\Security\UserSelectResource;
use IDRD\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class UserRolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        if (  request()->has('document') ) {
            $profiles = Profile::query()
                                 ->where('document', 'LIKE', "%".request()->get('document')."%")
                                 ->orWhere('full_name', 'LIKE', "%".request()->get('document')."%")
                                 ->take(20)
                                 ->get(['id'])->pluck('id')->toArray();
            $data = User::query()->whereIn('sim_id', $profiles)->get();
            return  $this->success_response( UserSelectResource::collection( $data ), 200 );
        }
        return response()->json([
            'data'  =>  [],
            'code'  =>  200
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return JsonResponse
     */
    public function show(User $user)
    {
        return response()->json([
            'data'  =>  $user->roles,
            'code'  =>  200
        ],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUserRolesRequest $request
     * @param User $user
     * @return JsonResponse
     */
    public function store(StoreUserRolesRequest $request, User $user)
    {
        $user->roles()->sync([]);
        $user->attachRoles( $request->get('roles') );
        return $this->success_message(__('validation.handler.success'), 201);
    }
}
