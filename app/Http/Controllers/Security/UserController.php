<?php

namespace IDRD\Http\Controllers\Security;

use IDRD\Entities\Security\Profile;
use IDRD\Entities\Security\User;
use IDRD\Http\Requests\Security\StoreUserRequest;
use IDRD\Http\Requests\Security\UpdateUserRequest;
use IDRD\Http\Resources\Security\UserResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;
use Throwable;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        if ( $this->query ) {
            $profiles = Profile::query()
                ->where( 'document', 'LIKE', "%{$this->query}%" )
                ->get(['id'])->pluck('id')->toArray();
        }
        $users = isset( $profiles ) ? User::query()->whereIn('sim_id', $profiles )->paginate( $this->per_page ) : $this->getQueryResponse( new User() );
        return $this->success_response(
            UserResource::collection( $users ),
            200
        );
    }

    /**
     * Display authenticated user profile
     *
     * @return UserResource
     */
    public function profile()
    {
        return new UserResource( request()->user() );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUserRequest $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function store(StoreUserRequest $request)
    {
        $user = new User();
        $user->password = bcrypt( $request->get('password') );
        $user->fill( $request->only([
            'name',
            'email',
            'username',
            'contract',
            'contract_initial_date',
            'contract_final_date',
            'virtual_record',
            'sim_id'
        ]));
        if ( $user->saveOrFail() ) {
            return $this->success_message(__('validation.handler.success'), 201);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return JsonResponse
     */
    public function show(User $user)
    {
        return $this->success_response(
            new UserResource( $user ),
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param User $user
     * @return JsonResponse
     * @throws Throwable
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $user->fill( $request->only([
            'name',
            'email',
            'username',
            'contract',
            'contract_initial_date',
            'contract_final_date',
            'virtual_record',
            'sim_id'
        ]));
        if ( $user->saveOrFail() ) {
            return $this->success_message(__('validation.handler.updated'), 200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $user->delete();
        return $this->success_message(__('validation.handler.deleted'), 200,204);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function restore($id)
    {
        $user = User::onlyTrashed()->find( $id );
        if ( $user->restore() ) {
            return $this->success_message(__('validation.handler.restored'), 200,200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }
}
