<?php

namespace IDRD\Http\Controllers\Security;

use IDRD\Entities\Security\Role;
use IDRD\Http\Requests\Security\StoreRoleRequest;
use IDRD\Http\Requests\Security\UpdateRoleRequest;
use IDRD\Http\Resources\Security\RoleResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;
use Illuminate\Http\Response;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->success_response(
            RoleResource::collection( $this->getQueryResponse( new Role()  ) ),
            200
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function all()
    {
        return $this->success_response(
            RoleResource::collection( Role::all() ),
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRoleRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function store(StoreRoleRequest $request)
    {
        $role = new Role();
        $role->fill( $request->only(['name', 'display_name', 'description']) );
        if ( $role->saveOrFail() ) {
            return $this->success_message(__('validation.handler.success'), 201);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Display the specified resource.
     *
     * @param Role $role
     * @return JsonResponse
     */
    public function show(Role $role)
    {
        return $this->success_response(
            new RoleResource( $role ),
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRoleRequest $request
     * @param Role $role
     * @return JsonResponse
     * @throws \Throwable
     */
    public function update(UpdateRoleRequest $request, Role $role)
    {
        $role->fill( $request->only(['name', 'display_name', 'description']) );
        if ( $role->saveOrFail() ) {
            return $this->success_message(__('validation.handler.updated'), 200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Role $role
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Role $role)
    {
        if ( $role->delete() ) {
            return $this->success_message(__('validation.handler.deleted'), 200,204);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function restore($id)
    {
        $role = Role::onlyTrashed()->find( $id );
        if ( $role->restore() ) {
            return $this->success_message(__('validation.handler.restored'), 200,204);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }
}
