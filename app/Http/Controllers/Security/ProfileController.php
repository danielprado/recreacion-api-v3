<?php

namespace IDRD\Http\Controllers\Security;

use IDRD\Entities\Security\Person;
use IDRD\Entities\Security\Profile;
use IDRD\Entities\Security\User;
use IDRD\Http\Requests\Security\CheckProfileRequest;
use IDRD\Http\Controllers\Controller;
use IDRD\Http\Requests\Security\StoreProfileRequest;
use IDRD\Http\Requests\Security\UpdateProfileRequest;
use IDRD\Http\Resources\Security\ProfileResource;
use IDRD\Http\Resources\Security\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function check(CheckProfileRequest $request)
    {
        $data = Profile::query()->where('document', $request->get('document'))->first();
        if ( isset($data->id) ) {
            $isRegistered = User::query()->where('sim_id', $data->id)->first();
            $user = isset( $isRegistered->id )
                ? new UserResource( $isRegistered )
                : new ProfileResource( $data );
            return $this->success_response($user, 200);
        }
    }

    public function store(StoreProfileRequest $request)
    {
        $person = new Person();
        $person = $this->fill( $person, $request );
        if ( $person->save() ) {
            return $this->success_message(
                __('validation.handler.success'),
                201,
                201,
                new ProfileResource( Profile::find( $person->Id_Persona ) )
            );
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    public function update(UpdateProfileRequest $request, Person $person)
    {
        if ( $this->fill( $person, $request )->saveOrFail() ) {
            return $this->success_message(
                __('validation.handler.updated'),
                201,
                201,
                new ProfileResource( Profile::find( $person->Id_Persona ) )
            );
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    public function fill(Person $person, Request $request)
    {
        $person->document = $request->get('document');
        $person->document_type_id = $request->get('document_type_id');
        $person->lastname = $request->get('lastname');
        $person->second_lastname = $request->get('second_lastname');
        $person->first_name = $request->get('first_name');
        $person->second_name = $request->get('second_name');
        $person->birthday = $request->get('birthday');
        $person->country_id = $request->get('country_id');
        $person->gender_id = $request->get('gender_id');
        $person->ethnicity_id = $request->get('ethnicity_id');
        $person->city_id = $request->get('city_id');

        return $person;
    }
}
