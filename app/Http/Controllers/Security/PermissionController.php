<?php

namespace IDRD\Http\Controllers\Security;

use IDRD\Entities\Security\Permission;
use IDRD\Http\Requests\Security\StorePermissionRequest;
use IDRD\Http\Requests\Security\UpdatePermissionRequest;
use IDRD\Http\Resources\Security\PermissionResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;
use Illuminate\Http\Response;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->success_response(
            PermissionResource::collection( $this->getQueryResponse( new Permission()  ) ),
            200
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function all()
    {
        return $this->success_response(
            PermissionResource::collection( Permission::all() ),
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePermissionRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function store(StorePermissionRequest $request)
    {
        $permission = new Permission();
        $permission->fill( $request->only(['name', 'display_name', 'description']) );
        if ( $permission->saveOrFail() ) {
            return $this->success_message(__('validation.handler.success'), 200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Display the specified resource.
     *
     * @param Permission $permission
     * @return JsonResponse
     */
    public function show(Permission $permission)
    {
        return $this->success_response(
            new PermissionResource( $permission ),
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePermissionRequest $request
     * @param Permission $permission
     * @return JsonResponse
     * @throws \Throwable
     */
    public function update(UpdatePermissionRequest $request, Permission $permission)
    {
        $permission->fill( $request->only(['name', 'display_name', 'description']) );
        if ( $permission->saveOrFail() ) {
            return $this->success_message(__('validation.handler.updated'), 200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Permission $permission
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Permission $permission)
    {
        if ( $permission->delete() ) {
            return $this->success_message(__('validation.handler.deleted'), 200,204);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function restore($id)
    {
        $permission = Permission::onlyTrashed()->find($id);
        if ( $permission->restore() ) {
            return $this->success_message(__('validation.handler.restored'), 200,204);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }
}
