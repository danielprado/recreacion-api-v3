<?php

namespace IDRD\Http\Controllers\Security;

use IDRD\Entities\Security\Country;
use IDRD\Http\Resources\Security\CountryResource;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;

class CountryController extends Controller
{
    public function index()
    {
        return $this->success_response(
            CountryResource::collection( Country::all() ),
            200
        );
    }
}
