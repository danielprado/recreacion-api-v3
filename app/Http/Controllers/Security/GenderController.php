<?php

namespace IDRD\Http\Controllers\Security;

use IDRD\Entities\Security\Gender;
use IDRD\Http\Resources\Security\GenderResource;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;

class GenderController extends Controller
{
    public function index()
    {
        return $this->success_response(
            GenderResource::collection( Gender::all() ),
            200
        );
    }
}
