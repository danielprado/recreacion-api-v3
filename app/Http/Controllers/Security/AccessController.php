<?php

namespace IDRD\Http\Controllers\Security;

use IDRD\Entities\Security\Access;
use IDRD\Entities\Security\Profile;
use IDRD\Http\Requests\Security\CheckAccessRequest;
use IDRD\Http\Requests\Security\StoreAccessRequest;
use IDRD\Http\Requests\Security\UpdateAccessRequest;
use IDRD\Http\Resources\Security\ProfileResource;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;

class AccessController extends Controller
{
    public function check(CheckAccessRequest $request)
    {
        $isRegistered = Access::query()->where('Id_Persona', $request->get('sim_id'))->count();
        $isTheSame = Access::query()->where('Id_Persona', $request->get('sim_id'))
                                    ->where('Usuario', toLower( $request->get('username') ))
                                    ->count();
        if ( $isRegistered > 0 && $isTheSame > 0) {
            return response()->json([
                'valid' => true,
                'data' => [
                    'message' => 'Username available'
                ]
            ], 200);
        } else {
            $usernameIsTaken = Access::query()->where('Usuario', toLower( $request->get('username') ))->count();
            if ( $usernameIsTaken > 0 ) {
                return response()->json([
                    'valid' => false,
                    'data' => [
                        'message' => __('validation.handler.username_taken')
                    ]
                ], 422);
            } else {
                return response()->json([
                    'valid' => true,
                    'data' => [
                        'message' => 'Username available'
                    ]
                ], 200);
            }
        }
    }

    public function store(StoreAccessRequest $request)
    {
        $access = new Access();
        $access->id       = $request->get('sim_id');
        $access->username = $request->get('username');
        $access->password = $request->get('password');
        if ( $access->saveOrFail() ) {
            return $this->success_message(
                __('validation.handler.success'),
                201,
                201,
                new ProfileResource( Profile::find( $request->get('sim_id') ) )
            );
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    public function update(UpdateAccessRequest $request, Access $access)
    {
        $access->username = $request->get('username');
        $access->password = $request->get('password');
        if ( $access->saveOrFail() ) {
            return $this->success_message(__('validation.handler.updated'), 200, 200, new ProfileResource( Profile::find( $access->id ) ));
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }
}
