<?php

namespace IDRD\Http\Controllers\Security;

use IDRD\Entities\Security\City;
use IDRD\Entities\Security\Country;
use IDRD\Http\Resources\Security\CityResource;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;

class CityController extends Controller
{
    public function index(Country $country)
    {
        $count =  City::query()->where('Id_Pais', $country->id)->count();
        if ( $count > 0 ) {
            $cities = City::query()->where('Id_Pais', $country->id)->get();
        } else {
            $cities = City::query()->where('Id_Ciudad', 1123)->get();
        }
        return $this->success_response(
            CityResource::collection( $cities ),
            200
        );
    }
}
