<?php

namespace IDRD\Http\Controllers\Security;

use IDRD\Entities\Security\DocumentType;
use IDRD\Http\Resources\Security\DocumentTypeResource;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;

class DocumentTypeController extends Controller
{
    public function index()
    {
        return $this->success_response(
            DocumentTypeResource::collection( DocumentType::all() ),
            200
        );
    }
}
