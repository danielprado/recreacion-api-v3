<?php

namespace IDRD\Http\Controllers\Scheme;

use Exception;
use IDRD\Entities\Scheme\Upz;
use IDRD\Http\Requests\Scheme\StoreUpzRequest;
use IDRD\Http\Requests\Scheme\UpdateUpzRequest;
use IDRD\Http\Resources\Scheme\NeighborhoodResource;
use IDRD\Http\Resources\Scheme\UpzResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Throwable;

class UpzController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->success_response(
          UpzResource::collection( $this->getQueryResponse( new Upz() ) )
        );
    }

    /**
     * Show the neighborhoods from upz.
     *
     * @param Upz $upz
     * @return JsonResponse
     */
    public function neighborhoods(Upz $upz)
    {
        $data = request()->has('paginated')
          ?  $upz->neighborhoods()->paginate( $this->per_page )
          :  $upz->neighborhoods;

        return $this->success_response(
          NeighborhoodResource::collection( $data ),
          200
        );
    }

  /**
   * Store a newly created resource in storage.
   *
   * @param StoreUpzRequest $request
   * @return JsonResponse
   * @throws Throwable
   */
    public function store(StoreUpzRequest $request)
    {
        $upz = new Upz();
        $upz->fill( $request->only(['name', 'upz_code', 'location_id']) );
        if ($upz->saveOrFail()) {
          return $this->success_message(__('validation.handler.success'), 201);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Display the specified resource.
     *
     * @param Upz $upz
     * @return JsonResponse
     */
    public function show(Upz $upz)
    {
        return $this->success_response(
          new UpzResource( $upz ),
          200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUpzRequest $request
     * @param Upz $upz
     * @return JsonResponse
     * @throws Throwable
     */
    public function update(UpdateUpzRequest $request, Upz $upz)
    {
        $upz->fill( $request->only(['name', 'upz_code', 'location_id']) );
        if ($upz->saveOrFail()) {
          return $this->success_message(__('validation.handler.updated'), 200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Upz $upz
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Upz $upz)
    {
        $upz->delete();
        return $this->success_message(__('validation.handler.deleted'), 200,204);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function restore($id)
    {
        $upz = Upz::onlyTrashed()->find( $id );
        if ( $upz->restore() ) {
          return $this->success_message(__('validation.handler.restored'), 200,200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }
}
