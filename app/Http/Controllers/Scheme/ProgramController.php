<?php

namespace IDRD\Http\Controllers\Scheme;

use IDRD\Entities\Scheme\Program;
use IDRD\Http\Requests\Scheme\StoreProgramRequest;
use IDRD\Http\Requests\Scheme\UpdateProgramRequest;
use IDRD\Http\Resources\Scheme\ActivityResource;
use IDRD\Http\Resources\Scheme\ProgramResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Throwable;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function all()
    {
        return $this->success_response(
            ProgramResource::collection( Program::all() ),
            200
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
      return $this->success_response(
        ProgramResource::collection( $this->getQueryResponse( new Program() ) ),
        200
      );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Program $program
     * @return JsonResponse
     */
    public function activities( Program $program )
    {
        $data = request()->has('paginated')
              ? $program->activities()->paginate( $this->per_page )
              : $program->activities;

        return $this->success_response(
            ActivityResource::collection( $data ),
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProgramRequest $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function store(StoreProgramRequest $request)
    {
        $program = new Program();
        $program->fill( $request->only(['name']) );
        if ($program->saveOrFail()) {
            return $this->success_message(__('validation.handler.success'), 201);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Display the specified resource.
     *
     * @param Program $program
     * @return JsonResponse
     */
    public function show(Program $program)
    {
        return $this->success_response(
            new ProgramResource( $program ),
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProgramRequest $request
     * @param Program $program
     * @return JsonResponse
     * @throws Throwable
     */
    public function update(UpdateProgramRequest $request, Program $program)
    {
        $program->fill( $request->only(['name']) );
        if ($program->saveOrFail()) {
            return $this->success_message(__('validation.handler.updated'), 201);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Program $program
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Program $program)
    {
        $program->delete();
        return $this->success_message(__('validation.handler.deleted'), 200,204);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function restore($id)
    {
        $program = Program::onlyTrashed()->find( $id );
        if ( $program->restore() ) {
            return $this->success_message(__('validation.handler.restored'), 200,200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }
}
