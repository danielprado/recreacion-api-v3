<?php

namespace IDRD\Http\Controllers\Scheme;

use Exception;
use IDRD\Entities\Scheme\Thematic;
use IDRD\Http\Requests\Scheme\StoreThematicRequest;
use IDRD\Http\Requests\Scheme\UpdateThematicRequest;
use IDRD\Http\Resources\Scheme\ComponentResource;
use IDRD\Http\Resources\Scheme\ThematicResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Throwable;

class ThematicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->success_response(
            ThematicResource::collection( $this->getQueryResponse( new Thematic() ) ),
            200
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Thematic $thematic
     * @return JsonResponse
     */
    public function components(Thematic $thematic)
    {
        $data = request()->has('paginated')
          ?  $thematic->components()->paginate( $this->per_page )
          :  $thematic->components;

        return $this->success_response(
            ComponentResource::collection( $data ),
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreThematicRequest $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function store(StoreThematicRequest $request)
    {
        $thematic = new Thematic();
        $thematic->fill( $request->only(['name', 'sub_activity_id']) );
        if ($thematic->saveOrFail()) {
            return $this->success_message(__('validation.handler.success'), 201);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Display the specified resource.
     *
     * @param Thematic $thematic
     * @return JsonResponse
     */
    public function show(Thematic $thematic)
    {
        return $this->success_response(
            new ThematicResource( $thematic ),
            200
        );
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateThematicRequest $request
     * @param Thematic $thematic
     * @return JsonResponse
     * @throws Throwable
     */
    public function update(UpdateThematicRequest $request, Thematic $thematic)
    {
        $thematic->fill( $request->only(['name', 'sub_activity_id']) );
        if ($thematic->saveOrFail()) {
            return $this->success_message(__('validation.handler.updated'), 200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Thematic $thematic
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Thematic $thematic)
    {
        $thematic->delete();
        return $this->success_message(__('validation.handler.deleted'), 200,204);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function restore($id)
    {
        $thematic = Thematic::onlyTrashed()->find( $id );
        if ( $thematic->restore() ) {
            return $this->success_message(__('validation.handler.restored'), 200,200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }
}
