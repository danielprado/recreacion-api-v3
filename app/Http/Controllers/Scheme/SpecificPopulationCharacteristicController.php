<?php

namespace IDRD\Http\Controllers\Scheme;

use IDRD\Entities\Scheme\SpecificPopulationCharacteristic;
use IDRD\Http\Requests\Scheme\StoreSpecificPopulationRequest;
use IDRD\Http\Requests\Scheme\UpdateSpecificPopulationRequest;
use IDRD\Http\Resources\Scheme\SpecificPopulationCharacteristicResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;
use Illuminate\Http\Response;

class SpecificPopulationCharacteristicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
      return $this->success_response(
        SpecificPopulationCharacteristicResource::collection( $this->getQueryResponse( new SpecificPopulationCharacteristic() ) ),
        200
      );
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param StoreSpecificPopulationRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function store(StoreSpecificPopulationRequest $request)
    {
        $population = new SpecificPopulationCharacteristic();
        $population->fill( $request->only(['name', 'population_id']) );
        if ($population->saveOrFail()) {
          return $this->success_message(__('validation.handler.success'), 201);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Display the specified resource.
     *
     * @param SpecificPopulationCharacteristic $specific_population
     * @return JsonResponse
     */
    public function show(SpecificPopulationCharacteristic $specific_population)
    {
        return $this->success_response(
          new SpecificPopulationCharacteristicResource( $specific_population ),
          200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSpecificPopulationRequest $request
     * @param SpecificPopulationCharacteristic $specific_population
     * @return JsonResponse
     * @throws \Throwable
     */
    public function update(UpdateSpecificPopulationRequest $request, SpecificPopulationCharacteristic $specific_population)
    {
        $specific_population->fill( $request->only(['name', 'population_id']) );
        if ($specific_population->saveOrFail()) {
          return $this->success_message(__('validation.handler.updated'), 200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param SpecificPopulationCharacteristic $specific_population
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(SpecificPopulationCharacteristic $specific_population)
    {
        $specific_population->delete();
        return $this->success_message(__('validation.handler.deleted'), 200,204);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function restore($id)
    {
        $population = SpecificPopulationCharacteristic::onlyTrashed()->find( $id );
        if ( $population->restore() ) {
          return $this->success_message(__('validation.handler.restored'), 200,200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }
}
