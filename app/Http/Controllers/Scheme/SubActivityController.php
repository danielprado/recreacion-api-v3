<?php

namespace IDRD\Http\Controllers\Scheme;

use IDRD\Entities\Scheme\SubActivity;
use IDRD\Http\Requests\Scheme\StoreSubActivityRequest;
use IDRD\Http\Requests\Scheme\UpdateSubActivityRequest;
use IDRD\Http\Resources\Scheme\SubActivityResource;
use IDRD\Http\Resources\Scheme\ThematicResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;
use Illuminate\Http\Response;

class SubActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->success_response(
            SubActivityResource::collection( $this->getQueryResponse( new SubActivity() ) ),
            200
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param SubActivity $sub_activity
     * @return JsonResponse
     */
    public function thematics(SubActivity $sub_activity)
    {
        $data = request()->has('paginated')
          ?  $sub_activity->thematics()->paginate( $this->per_page )
          :  $sub_activity->thematics;

        return $this->success_response(
            ThematicResource::collection( $data ),
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreSubActivityRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function store(StoreSubActivityRequest $request)
    {
        $sub_activity = new SubActivity();
        $sub_activity->fill( $request->only(['name', 'activity_id']) );
        if ($sub_activity->saveOrFail()) {
            return $this->success_message(__('validation.handler.success'), 201);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Display the specified resource.
     *
     * @param SubActivity $sub_activity
     * @return JsonResponse
     */
    public function show(SubActivity $sub_activity)
    {
        return $this->success_response(
            new SubActivityResource( $sub_activity ),
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSubActivityRequest $request
     * @param SubActivity $sub_activity
     * @return JsonResponse
     * @throws \Throwable
     */
    public function update(UpdateSubActivityRequest $request, SubActivity $sub_activity)
    {
        $sub_activity->fill( $request->only(['name', 'activity_id']) );
        if ($sub_activity->saveOrFail()) {
            return $this->success_message(__('validation.handler.success'), 200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param SubActivity $sub_activity
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(SubActivity $sub_activity)
    {
        $sub_activity->delete();
        return $this->success_message(__('validation.handler.deleted'), 200,204);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function restore($id)
    {
        $sub_activity = SubActivity::onlyTrashed()->find( $id );
        if ( $sub_activity->restore() ) {
            return $this->success_message(__('validation.handler.restored'), 200,200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }
}
