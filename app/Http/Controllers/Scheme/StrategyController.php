<?php

namespace IDRD\Http\Controllers\Scheme;

use IDRD\Entities\Scheme\Strategy;
use IDRD\Http\Requests\Scheme\StoreStrategyRequest;
use IDRD\Http\Requests\Scheme\UpdateStrategyRequest;
use IDRD\Http\Resources\Scheme\StrategyResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Throwable;

class StrategyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->success_response(
            StrategyResource::collection( $this->getQueryResponse( new Strategy() ) ),
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreStrategyRequest $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function store(StoreStrategyRequest $request)
    {
        $strategy = new Strategy();
        $strategy->fill( $request->only(['name', 'component_id']) );
        if ($strategy->saveOrFail()) {
            return $this->success_message(__('validation.handler.success'), 201);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Display the specified resource.
     *
     * @param Strategy $strategy
     * @return JsonResponse
     */
    public function show(Strategy $strategy)
    {
        return $this->success_response(
            new StrategyResource( $strategy ),
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateStrategyRequest $request
     * @param Strategy $strategy
     * @return JsonResponse
     * @throws Throwable
     */
    public function update(UpdateStrategyRequest $request, Strategy $strategy)
    {
        $strategy->fill( $request->only(['name', 'component_id']) );
        if ($strategy->saveOrFail()) {
            return $this->success_message(__('validation.handler.updated'), 200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Strategy $strategy
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Strategy $strategy)
    {
        $strategy->delete();
        return $this->success_message(__('validation.handler.deleted'), 200,204);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function restore($id)
    {
        $strategy = Strategy::onlyTrashed()->find($id);
        if ( $strategy->restore() ) {
            return $this->success_message(__('validation.handler.restored'), 200,200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }
}
