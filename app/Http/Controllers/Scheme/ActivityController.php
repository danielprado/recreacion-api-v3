<?php

namespace IDRD\Http\Controllers\Scheme;

use IDRD\Entities\Scheme\Activity;
use IDRD\Http\Requests\Scheme\StoreActivityRequest;
use IDRD\Http\Requests\Scheme\UpdateActivityRequest;
use IDRD\Http\Resources\Scheme\ActivityResource;
use IDRD\Http\Resources\Scheme\SubActivityResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Throwable;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->success_response(
            ActivityResource::collection( $this->getQueryResponse( new Activity() ) ),
            200
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Activity $activity
     * @return JsonResponse
     */
    public function sub_activities(Activity $activity)
    {
        $data = request()->has('paginated')
          ? $activity->sub_activities()->paginate( $this->per_page )
          : $activity->sub_activities;

        return $this->success_response(
            SubActivityResource::collection( $data ),
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreActivityRequest $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function store(StoreActivityRequest $request)
    {
        $activity = new Activity();
        $activity->fill( $request->only(['name', 'program_id']) );
        if ($activity->saveOrFail()) {
            return $this->success_message(__('validation.handler.success'), 201);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Display the specified resource.
     *
     * @param Activity $activity
     * @return JsonResponse
     */
    public function show(Activity $activity)
    {
        return $this->success_response(
            new ActivityResource( $activity ),
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateActivityRequest $request
     * @param Activity $activity
     * @return JsonResponse
     * @throws Throwable
     */
    public function update(UpdateActivityRequest $request, Activity $activity)
    {
        $activity->fill( $request->only(['name', 'program_id']) );
        if ($activity->saveOrFail()) {
            return $this->success_message(__('validation.handler.updated'), 201);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Activity $activity
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Activity $activity)
    {
        $activity->delete();
        return $this->success_message(__('validation.handler.deleted'), 200,204);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function restore($id)
    {
        $activity = Activity::onlyTrashed()->find( $id );
        if ( $activity->restore() ) {
            return $this->success_message(__('validation.handler.restored'), 200,200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }
}
