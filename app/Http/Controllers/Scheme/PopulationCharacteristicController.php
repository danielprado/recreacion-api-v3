<?php

namespace IDRD\Http\Controllers\Scheme;

use Exception;
use IDRD\Entities\Scheme\PopulationCharacteristic;
use IDRD\Http\Requests\Scheme\StorePopulationRequest;
use IDRD\Http\Requests\Scheme\UpdatePopulationRequest;
use IDRD\Http\Resources\Scheme\PopulationCharacteristicResource;
use IDRD\Http\Resources\Scheme\SpecificPopulationCharacteristicResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;
use Illuminate\Http\Response;

class PopulationCharacteristicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function all()
    {
      return $this->success_response(
        PopulationCharacteristicResource::collection( PopulationCharacteristic::all() ),
        200
      );
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
      return $this->success_response(
        PopulationCharacteristicResource::collection( $this->getQueryResponse( new PopulationCharacteristic() ) ),
        200
      );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param PopulationCharacteristic $population
     * @return JsonResponse
     */
    public function specifics( PopulationCharacteristic $population )
    {
      $data = request()->has('paginated')
        ? $population->specifics()->paginate( $this->per_page )
        : $population->specifics;

      return $this->success_response(
        SpecificPopulationCharacteristicResource::collection( $data ),
        200
      );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePopulationRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function store(StorePopulationRequest $request)
    {
        $population = new PopulationCharacteristic();
        $population->fill( $request->only(['name']) );
        if ($population->saveOrFail()) {
          return $this->success_message(__('validation.handler.success'), 201);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Display the specified resource.
     *
     * @param PopulationCharacteristic $population
     * @return JsonResponse
     */
    public function show(PopulationCharacteristic $population)
    {
        return $this->success_response(
          new PopulationCharacteristicResource( $population ),
          200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePopulationRequest $request
     * @param PopulationCharacteristic $population
     * @return JsonResponse
     * @throws \Throwable
     */
    public function update(UpdatePopulationRequest $request, PopulationCharacteristic $population)
    {
        $population->fill( $request->only(['name']) );
        if ($population->saveOrFail()) {
          return $this->success_message(__('validation.handler.updated'), 200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param PopulationCharacteristic $population
     * @return JsonResponse
     * @throws Exception
    */
    public function destroy(PopulationCharacteristic $population)
    {
        $population->delete();
        return $this->success_message(__('validation.handler.deleted'), 200,204);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function restore($id)
    {
        $population = PopulationCharacteristic::onlyTrashed()->find( $id );
        if ( $population->restore() ) {
          return $this->success_message(__('validation.handler.restored'), 200,200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }
}
