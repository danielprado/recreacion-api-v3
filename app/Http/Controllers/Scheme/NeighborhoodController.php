<?php

namespace IDRD\Http\Controllers\Scheme;

use Exception;
use IDRD\Entities\Scheme\Neighborhood;
use IDRD\Http\Requests\Scheme\StoreNeighborhoodRequest;
use IDRD\Http\Requests\Scheme\UpdateNeighborhoodRequest;
use IDRD\Http\Resources\Scheme\NeighborhoodResource;
use Illuminate\Http\JsonResponse;
use IDRD\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Throwable;

class NeighborhoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->success_response(
          NeighborhoodResource::collection( $this->getQueryResponse( new Neighborhood() ) )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreNeighborhoodRequest $request
     * @return JsonResponse|Response
     * @throws Throwable
     */
    public function store(StoreNeighborhoodRequest $request)
    {
        $neighborhood = new Neighborhood();
        $neighborhood->fill( $request->only(['name', 'upz_id']) );
        if ($neighborhood->saveOrFail()) {
          return $this->success_message(__('validation.handler.success'), 201);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Display the specified resource.
     *
     * @param Neighborhood $neighborhood
     * @return JsonResponse
     */
    public function show(Neighborhood $neighborhood)
    {
      return $this->success_response(
        new NeighborhoodResource( $neighborhood ),
        200
      );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateNeighborhoodRequest $request
     * @param Neighborhood $neighborhood
     * @return JsonResponse
     * @throws Throwable
     */
    public function update(UpdateNeighborhoodRequest $request, Neighborhood $neighborhood)
    {
        $neighborhood->fill( $request->only(['name', 'upz_id']) );
        if ($neighborhood->saveOrFail()) {
          return $this->success_message(__('validation.handler.updated'), 200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Neighborhood $neighborhood
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Neighborhood $neighborhood)
    {
      $neighborhood->delete();
      return $this->success_message(__('validation.handler.deleted'), 200,204);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function restore($id)
    {
      $neighborhood = Neighborhood::onlyTrashed()->find( $id );
      if ( $neighborhood->restore() ) {
        return $this->success_message(__('validation.handler.restored'), 200,200);
      }
      return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }
}
