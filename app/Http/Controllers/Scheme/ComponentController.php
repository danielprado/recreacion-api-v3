<?php

namespace IDRD\Http\Controllers\Scheme;

use IDRD\Entities\Scheme\Component;
use IDRD\Http\Requests\Scheme\StoreComponentRequest;
use IDRD\Http\Requests\Scheme\UpdateComponentRequest;
use IDRD\Http\Resources\Scheme\ComponentResource;
use IDRD\Http\Resources\Scheme\StrategyResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ComponentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->success_response(
            ComponentResource::collection( $this->getQueryResponse( new Component() ) ),
            200
        );
    }

  /**
   * Show the form for creating a new resource.
   *
   * @param Component $component
   * @return JsonResponse
   */
    public function strategies(Component $component)
    {
        $data = request()->has('paginated')
          ? $component->strategies()->paginate( $this->per_page )
          : $component->strategies;

        return $this->success_response(
            StrategyResource::collection( $data),
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreComponentRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function store(StoreComponentRequest $request)
    {
        $component = new Component();
        $component->fill( $request->only(['name', 'thematic_id']) );
        if ($component->saveOrFail()) {
            return $this->success_message(__('validation.handler.success'), 201);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Display the specified resource.
     *
     * @param Component $component
     * @return JsonResponse
     */
    public function show(Component $component)
    {
        return $this->success_response(
            new ComponentResource( $component ),
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateComponentRequest $request
     * @param Component $component
     * @return JsonResponse
     * @throws \Throwable
     */
    public function update(UpdateComponentRequest $request, Component $component)
    {
        $component->fill( $request->only(['name', 'thematic_id']) );
        if ($component->saveOrFail()) {
            return $this->success_message(__('validation.handler.updated'), 200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Component $component
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Component $component)
    {
        $component->delete();
        return $this->success_message(__('validation.handler.deleted'), 200,204);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function restore($id)
    {
        $component = Component::onlyTrashed()->find( $id );
        if ( $component->restore() ) {
            return $this->success_message(__('validation.handler.restored'), 200,200);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }
}
