<?php

namespace IDRD\Http\Controllers\Scheme;

use Exception;
use IDRD\Entities\Scheme\Location;
use IDRD\Http\Requests\Scheme\StoreLocationRequest;
use IDRD\Http\Requests\Scheme\UpdateLocationRequest;
use IDRD\Http\Resources\Scheme\LocationResource;
use IDRD\Http\Resources\Scheme\UpzResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use IDRD\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Throwable;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->success_response(
          LocationResource::collection( $this->getQueryResponse( new Location() ) )
        );
    }

    /**
     * Show the upzs from locations.
     *
     * @param Location $location
     * @return JsonResponse
     */
    public function upz(Location $location)
    {
        $data = request()->has('paginated')
          ? $location->upzs()->paginate( $this->per_page )
          : $location->upzs;

        return $this->success_response(
          UpzResource::collection( $data ),
          200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreLocationRequest $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function store(StoreLocationRequest $request)
    {
        $location = new Location();
        $location->fill( $request->only(['name']) );
        if ($location->saveOrFail()) {
          return $this->success_message(__('validation.handler.success'), 201);
        }
        return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Display the specified resource.
     *
     * @param Location $location
     * @return JsonResponse
     */
    public function show(Location $location)
    {
      return $this->success_response(
        new LocationResource( $location ),
        200
      );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateLocationRequest $request
     * @param Location $location
     * @return JsonResponse
     * @throws Throwable
     */
    public function update(UpdateLocationRequest $request, Location $location)
    {
      $location->fill( $request->only(['name']) );
      if ($location->saveOrFail()) {
        return $this->success_message(__('validation.handler.updated'), 200);
      }
      return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Location $location
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Location $location)
    {
      $location->delete();
      return $this->success_message(__('validation.handler.deleted'), 200,204);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function restore($id)
    {
      $location = Location::onlyTrashed()->find( $id );
      if ( $location->restore() ) {
        return $this->success_message(__('validation.handler.restored'), 200,200);
      }
      return $this->error_response(__('validation.handler.unexpected_failure'), 422);
    }
}
