<?php

namespace IDRD\Http\Controllers;

use IDRD\Rules\OrderBy;
use IDRD\Traits\ApiResponse;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Builder;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, ApiResponse;

    /**
     * The attribute to include into pagination resources
     *
     * @var int
     */
    protected $per_page;
    /**
     * The attribute to include into order function
     *
     * @var string
     */
    protected $order_by;
    /**
     * The attribute to order collection ascending or descending
     *
     * @var string
     */
    protected $direction;
    /**
     * The attribute to order collection ascending or descending
     *
     * @var string
     */
    protected $query;
    /**
     * The attribute to include relations
     *
     * @var string
     */
    protected $with;
    /**
     * The attribute to trashed data
     *
     * @var string
     */
    protected $trashed;
    /**
     * The attribute to only trashed data
     *
     * @var string
     */
    protected $only_trashed;
    /**
     * The attribute to include relations
     *
     * @var string
     */
    protected $not;
    /**
     * ApiController constructor.
     */
    public function __construct()
    {
        Validator::validate( request()->all(), [
            'query'      =>  'nullable|string',
            'per_page'   =>  'nullable|integer|min:2|max:100',
            'column'     =>  'nullable|array',
            'column.*'   =>  'nullable|string|min:2|max:20',
            'order'      =>  ['nullable', 'array'],
            'with'       =>  'nullable|string',
            'trashed'    =>  'nullable|string',
            'only_trashed'=>  'nullable|string',
            'not'        =>  'nullable|string',
        ]);
        $this->middleware('auth:api');
        $this->per_page = $this->getPerPageAttribute();
        $this->order_by = $this->getOrderByAttribute();
        $this->direction = $this->getDirectionAttribute();
        $this->query = $this->getQueryAttribute();
        $this->with = $this->getWithAttribute();
        $this->trashed = $this->getTrashedAttribute();
        $this->only_trashed = $this->getOnlyTrashedAttribute();
    }

    /**
     * Check and get the pagination quantity number
     *
     * @return int
     */
    protected function getPerPageAttribute()
    {
        return ( request()->has('per_page') ) ? (int) request()->get('per_page') : 10;
    }

    /**
     * Check and get the data ordering by specific column
     *
     * @return string
     */
    protected function getOrderByAttribute()
    {
        return ( request()->has('column') && isset( request()->get('column')[0] ) )
                ? (string) request()->get('column')[0]
                : 'id';
    }

    /**
     * Check and get the data ordering direction
     *
     * @return string
     */
    protected function getDirectionAttribute()
    {
        return ( request()->has('order') && isset( request()->get('order')[0] ) )
                ? request()->get('order')[0]  == 'true'
                : false;
    }

    /**
     * Check and get the data ordering direction
     *
     * @return string
     */
    protected function getQueryAttribute()
    {
        return ( request()->has('query') ) ? (string) request()->get('query') : null;
    }

    /**
     * Check and get the data with trashed data
     *
     * @return string
     */
    protected function getTrashedAttribute()
    {
        return ( request()->has('trashed') )
               ? ( request()->get('trashed') == 'true' )
               : false;
    }

    /**
     * Check and get the data with trashed data
     *
     * @return string
     */
    protected function getOnlyTrashedAttribute()
    {
        return ( request()->has('only_trashed') )
               ? ( request()->get('only_trashed') == 'true' )
               : false;
    }

    /**
     * Check and get the data with related models
     *
     * @return string
     */
    protected function getWithAttribute()
    {
        return ( request()->has('with') ) ? (string) request()->get('with') : null;
    }

    /**
     * @param $query
     * @return bool
     */
    protected function queryHasPipe($query )
    {
        return ( strpos( $query, '|') === false ) ? false : true;
    }

    /**
     * @param $query
     * @return bool
     */
    protected function queryHasNotPipe($query )
    {
        return ! $this->queryHasPipe( $query );
    }

    /**
     * @param $column
     * @param $table
     * @return mixed
     */
    protected function modelHasColumn($column, $table )
    {
        return Schema::hasColumn($table, $column);
    }

    /**
     * @param $collection
     * @return array
     */
    protected function getQueryRelations( $collection )
    {
        $relations = ( isset( $this->with ) && count( explode('|', $this->with) ) > 0 )
            ? explode('|', $this->with)
            : [];
        if ( is_array( $relations ) && count( $relations ) > 0 ) {
            if ( $collection instanceof Builder) {
                $model = $collection->getModel();
                return $this->loadRelations( $relations, $model );
            }
            if ( $collection instanceof Model ) {
                return $this->loadRelations( $relations, $collection );
            }
        }
    }

    /**
     * @param $relations
     * @param Model $model
     * @return array
     */
    protected function loadRelations($relations, Model $model)
    {
        foreach ( $relations as $relation ) {
            // if ( array_has( $model->getRelations(), $relation) ) {
            $data[] = $relation;
            // }
        }
        return isset( $data ) ? $data : [];
    }

    /**
     * @param Model $model
     * @param array $relations
     * @param bool $isPivot
     * @return mixed
     */
    protected function getModel(Model $model, array $relations = [], $isPivot = false)
    {
        $order_by = ( $this->modelHasColumn( $this->order_by, $model->getTable() ) ) ? $this->order_by : 'id';
        $collection = ( $this->direction ) ? $model->orderBy( $order_by ) : $model->orderByDesc( $order_by );
        if (!$isPivot) {
            if ( $this->query && $this->queryHasNotPipe( $this->query ) ) {
                foreach ( Schema::getColumnListing( $model->getTable() ) as $column ) {
                    $collection->orWhere( $column, 'LIKE', '%' . $this->query . '%' );
                }
            } else {
                $query = explode('|', $this->query);
                $column = isset( $query[0] ) ? $query[0] : 'id';
                $value = isset( $query[1] ) ? $query[1] : '';
                $collection = ( $this->modelHasColumn( $column, $model->getTable() ) )
                    ? $collection->where( $column, 'LIKE', '%' . $value . '%' )
                    : $collection;
            }
        }
        if ( count( $relations ) > 0  ) {
            $collection = $collection->with( $relations );
        }

        if ( $this->trashed && !$this->only_trashed ) {
            $collection = $collection->withTrashed();
        }

        if ( $this->only_trashed ) {
          $collection = $collection->onlyTrashed();
        }

        return $collection->paginate( $this->per_page );
    }


    /**
     * @param Builder $builder
     * @param array $relations
     * @param bool $isPivot
     * @return mixed
     */
    protected function getBuilder(Builder $builder, array $relations = [], $isPivot = false)
    {
        $model = $builder->getModel();
        $order_by = ( $this->modelHasColumn( $this->order_by, $model->getTable() ) ) ? $this->order_by : 'id';
        $collection = ( $this->direction ) ? $builder->orderBy( $order_by ) : $builder->orderByDesc( $order_by );
        if (!$isPivot) {
            if ( $this->query && $this->queryHasNotPipe( $this->query ) ) {
                foreach ( Schema::getColumnListing( $model->getTable() ) as $column ) {
                    $collection->orWhere( $column, 'LIKE', '%' . $this->query . '%' );
                }
            } else {
                $query = explode('|', $this->query);
                $column = isset( $query[0] ) ? $query[0] : 'id';
                $value = isset( $query[1] ) ? $query[1] : '';
                $collection = ( $this->modelHasColumn( $column, $model->getTable() ) )
                    ? $collection->where( $column, 'LIKE', '%' . $value . '%' )
                    : $collection;
            }
        }
        if ( count( $relations ) > 0 ) {
            $collection = $collection->with( $relations );
        }

        if ( $this->trashed ) {
            $collection = $collection->withTrashed();
        }

        return $collection->paginate( $this->per_page );
    }


    /**
     * @param $collection
     * @param array $relations
     * @param bool $isPivot
     * @return mixed
     */
    protected function getQueryResponse($collection, array $relations = [], $isPivot = false )
    {
        if ( is_array( $this->getQueryRelations( $collection ) ) && count( $this->getQueryRelations( $collection ) ) > 0 ) {
            $relations =  array_flatten( array_unique( array_merge_recursive( $relations, $this->getQueryRelations( $collection ) ) ) );
        }
        if ( $collection instanceof Builder) {
            return $this->getBuilder( $collection, $relations, $isPivot );
        }
        if ( $collection instanceof Model ) {
            return $this->getModel( $collection, $relations, $isPivot );
        }
    }
}
