<?php

namespace IDRD\Imports;

use IDRD\Entities\Security\Access;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class SimImport implements ToModel, WithHeadingRow, WithValidation, SkipsOnFailure
{
    use Importable, SkipsFailures;
    /**
    * @param array $row
    *
    * @return Model|null
    */
    public function model(array $row)
    {
        return new Access([
            'Usuario'     =>  isset( $row['username'] ) ? $row['username'] : null,
            'Contrasena'  =>  isset( $row['password'] ) ? $row['password'] : null,
        ]);
    }

  public function rules(): array
  {
    return [
      'username' => function($attribute, $value, $onFailure) {
        if (is_null( $value ) || $value == "") {
          $onFailure( trans('validation.required', trans('validation.attributes.username')) );
        }
      },
      'password' => function($attribute, $value, $onFailure) {
        if (is_null( $value ) || $value == "") {
          $onFailure( trans('validation.required', trans('validation.attributes.password')) );
        }
      }
    ];
  }
}
