<?php

namespace IDRD\Entities\Security;

use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as Auditor;
use Zizaco\Entrust\EntrustPermission;

class Permission  extends EntrustPermission implements Auditable
{
    use SoftDeletes, Auditor;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'display_name', 'description',
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name'          =>  'string',
        'display_name'  =>  'string',
        'description'   =>  'string',
    ];
    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'name',
        'display_name',
        'description',
    ];
    /**
     * Generating tags for each model audited.
     *
     * @return array
     */
    public function generateTags() : array
    {
        return ['permission'];
    }

    /*
    * ---------------------------------------------------------
    * Accessors and Mutator
    * ---------------------------------------------------------
    */

    public function setNameAttribute( $value )
    {
        $this->attributes['name'] = toLower($value);
    }

    public function setDisplayNameAttribute( $value )
    {
        $this->attributes['display_name'] = toTitle($value);
    }

    public function setDescriptionAttribute( $value )
    {
        $this->attributes['description'] = toFirstUpper($value);
    }
}
