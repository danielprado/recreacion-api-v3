<?php

namespace IDRD\Entities\Security;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as Auditor;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable implements Auditable
{
    use Notifiable, HasApiTokens, SoftDeletes, Auditor, EntrustUserTrait {
        EntrustUserTrait::restore insteadof SoftDeletes;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'username',
        'password',
        'contract',
        'contract_initial_date',
        'contract_final_date',
        'virtual_record',
        'sim_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at'     => 'datetime',
        'contract_initial_date' => 'date',
        'contract_final_date'   => 'date',
        'sim_id'                => 'int',
    ];

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'name',
        'email',
        'username',
        'contract',
        'contract_initial_date',
        'contract_final_date',
        'virtual_record',
        'sim_id',
    ];

    /**
     * Attributes to exclude from the Audit.
     *
     * @var array
     */
    protected $auditExclude = [
        'password',
    ];

    /**
     * Generating tags for each model audited.
     *
     * @return array
     */
    public function generateTags() : array
    {
        return ['user'];
    }

    /*
     * Helps Passport Login users using their username
     */
    public function findForPassport($username)
    {
        return $this->where('username', $username)
                    //->whereDate('contract_initial_date', '<=', Carbon::now() )
                    //->whereDate('contract_final_date', '>=', Carbon::now() )
                    ->first();
    }

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator 
     * ---------------------------------------------------------
     */

    public function setNameAttribute( $value )
    {
        $this->attributes['name'] = toUpper($value);
    }

    public function setEmailAttribute( $value ) {
        $this->attributes['email'] = toLower($value);
    }

    public function setUsernameAttribute( $value ) {
        $this->attributes['username'] = toLower($value);
    }

    public function setContractAttribute( $value ) {
        $this->attributes['contract'] = toUpper($value);
    }
    public function setVirtualRecordAttribute( $value ) {
        $this->attributes['virtual_record'] = toUpper($value);
    }
    
    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * Get the user profile.
     *
     * @return HasOne
     */
    public function profile()
    {
        return $this->hasOne( Profile::class, 'id', 'sim_id' );
    }
}
