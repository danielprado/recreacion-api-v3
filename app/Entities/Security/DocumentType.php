<?php

namespace IDRD\Entities\Security;

use Illuminate\Database\Eloquent\Model;

class DocumentType extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_users';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tipo_documento';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id_TipoDocumento';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Nombre_TipoDocumento', 'Descripcion_TipoDocumento'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Get the document type's id.
     *
     * @return int
     */
    public function getIdAttribute()
    {
        return (int) $this->Id_TipoDocumento;
    }

    /**
     * Get the document type's name.
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return  $this->Nombre_TipoDocumento;
    }

    /**
     * Get the document type's description.
     *
     * @return string
     */
    public function getDescriptionAttribute()
    {
        return  $this->Descripcion_TipoDocumento;
    }
}
