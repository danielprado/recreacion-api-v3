<?php


namespace IDRD\Entities\Security;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use TaylorNetwork\UsernameGenerator\FindSimilarUsernames;
use TaylorNetwork\UsernameGenerator\GeneratesUsernames;

class Access extends Model
{

    use FindSimilarUsernames, GeneratesUsernames;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_users';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'acceso';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id_Persona';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Usuario', 'Contrasena'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['Contrasena', 'password'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Get the user's id.
     *
     * @return int
     */
    public function getIdAttribute()
    {
        return (int) $this->Id_Persona;
    }

    /**
     * Get the user's username.
     *
     * @return int
     */
    public function getUsernameAttribute()
    {
        return $this->Usuario;
    }

    /**
     * Get the user's password.
     *
     * @return int
     */
    public function getPasswordAttribute()
    {
        return $this->Contrasena;
    }

    /**
     * Set the user's id.
     *
     * @param  string  $value
     * @return string
     */
    public function setIdAttribute($value)
    {
        $this->attributes['Id_Persona'] = $value;
    }

    /**
     * Set the user's username.
     *
     * @param  string  $value
     * @return string
     */
    public function setUsernameAttribute($value)
    {
        $this->attributes['Usuario'] = toLower($value);
    }

    /**
     * Set the user's password.
     *
     * @param  string  $value
     * @return string
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['Contrasena'] = $this->hash( $value );
    }

    /**
     * Set the user's username.
     *
     * @param  string  $value
     * @return string
     */
    public function setUsuarioAttribute($value)
    {
        $this->attributes['Usuario'] = toLower($value);
    }

    /**
     * Set the user's password.
     *
     * @param  string  $value
     * @return string
     */
    public function setContrasenaAttribute($value)
    {
        $this->attributes['Contrasena'] = $this->hash( $value );
    }

    /**
     * Generate the username and save to model.
     */
    public function generatorConfig(&$generator)
    {
      $generator->setConfig([ 'separator' => '.', 'case'  =>  'lower', 'column' => 'Usuario', ]);
    }

    /**
     * Get the field attribute to convert to username.
     *
     * Override this method in your model to customize logic.
     *
     * @return string
     */
    public function getField(): string
    {
      return $this->Usuario;
    }

    /**
     * Hash the password
     *
     * @param $password
     * @return string
     */
    public function hash( $password )
    {
        $k = 18;
        $C = '';
        for( $i=0; $i < strlen( $password ) ; $i++ ) $C.=chr((ord( $password [$i])+$k)%255);
        return sha1( $C );
    }


    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * Get the user profile.
     *
     * @return HasOne
     */
    public function profile()
    {
        return $this->hasOne( Profile::class, 'id', 'Id_Persona' );
    }
}
