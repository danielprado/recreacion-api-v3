<?php


namespace IDRD\Entities\Security;


use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_users';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'persona';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id_Persona';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Cedula',
        'Id_TipoDocumento',
        'Primer_Apellido',
        'Segundo_Apellido',
        'Primer_Nombre',
        'Segundo_Nombre',
        'Fecha_Nacimiento',
        'Id_Pais',
        'Nombre_Ciudad',
        'Id_Genero',
        'Id_Etnia',
        'Id_Departamento',
        'i_fk_id_ciudad',
        'Id_Identidad_Genero',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Get the user's id.
     *
     * @return int
     */
    public function getIdAttribute()
    {
        return (int) $this->Id_Persona;
    }

    /**
     * Set the user's document.
     *
     * @param  string  $value
     * @return string
     */
    public function setIdAttribute($value)
    {
        $this->attributes['Id_Persona'] = (int) $value;
    }

    /**
     * Set the user's document.
     *
     * @param  string  $value
     * @return string
     */
    public function setDocumentAttribute($value)
    {
        $this->attributes['Cedula'] = toLower($value);
    }

    /**
     * Set the user's document type id.
     *
     * @param  int  $value
     */
    public function setDocumentTypeIdAttribute($value)
    {
        $this->attributes['Id_TipoDocumento'] = (int) $value;
    }

    /**
     * Set user lastname
     *
     * @param string $value
     */
    public function setLastnameAttribute($value){
        $this->attributes['Primer_Apellido'] = toUpper($value); //lastname
    }

    /**
     * Set user second lastname
     *
     * @param string $value
     */
    public function setSecondLastnameAttribute($value){
        $this->attributes['Segundo_Apellido'] = toUpper($value); //second_lastname
    }

    /**
     * Set user first name
     *
     * @param string $value
     */
    public function setFirstNameAttribute($value){
        $this->attributes['Primer_Nombre'] = toUpper($value); //first_name
    }

    /**
     * Set user second name
     *
     * @param string $value
     */
    public function setSecondNameAttribute($value){
        $this->attributes['Segundo_Nombre'] = toUpper($value); //second_name
    }

    /**
     * Set user birthday
     *
     * @param string $value
     */
    public function setBirthdayAttribute($value){
        $this->attributes['Fecha_Nacimiento'] = $value;
    }

    /**
     * Set user country
     *
     * @param int $value
     */
    public function setCountryIdAttribute($value){
        $this->attributes['Id_Pais'] = (int) $value;
    }

    /**
     * Set user city name
     *
     * @param string $value
     */
    public function setCityNameAttribute($value){
        $this->attributes['Nombre_Ciudad'] = toUpper($value);
    }

    /**
     * Set user gender
     *
     * @param int $value
     */
    public function setGenderIdAttribute($value){
        $this->attributes['Id_Genero'] = (int) $value;
    }

    /**
     * Set user ethnicity
     *
     * @param int $value
     */
    public function setEthnicityIdAttribute($value){
        $this->attributes['Id_Etnia'] = (int) $value;
    }

    /**
     * Set user department id
     *
     * @param int $value
     */
    public function setDepartmentIdAttribute($value){
        $this->attributes['Id_Departamento'] = (int) $value;
    }

    /**
     * Set user city id
     *
     * @param int $value
     */
    public function setCityIdAttribute($value){
        $this->attributes['i_fk_id_ciudad'] = (int) $value;
    }

    /**
     * Set user gender identity id
     *
     * @param string $value
     */
    public function setGenderIdentityIdAttribute($value){
        $this->attributes['Id_Identidad_Genero'] = (int) $value;
    }
}