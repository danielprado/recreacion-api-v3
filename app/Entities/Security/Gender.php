<?php

namespace IDRD\Entities\Security;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_users';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'genero';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id_Genero';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Nombre_Genero'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Get the id.
     *
     * @return int
     */
    public function getIdAttribute()
    {
        return (int) $this->Id_Genero;
    }

    /**
     * Get the name.
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->Nombre_Genero;
    }
}
