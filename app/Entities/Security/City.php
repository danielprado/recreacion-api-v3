<?php

namespace IDRD\Entities\Security;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_users';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ciudad';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id_Ciudad';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Nombre_Ciudad', 'Id_Pais'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Get the id.
     *
     * @return int
     */
    public function getIdAttribute()
    {
        return (int) $this->Id_Ciudad;
    }

    /**
     * Get the city name.
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return  $this->Nombre_Ciudad;
    }

    /**
     * Get the country id.
     *
     * @return int
     */
    public function getCountryIdAttribute()
    {
        return (int) $this->Id_Pais;
    }
}
