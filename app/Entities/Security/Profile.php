<?php

namespace IDRD\Entities\Security;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_users';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'view_users_profile';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'document',
        'document_type_id',
        'document_type',
        'document_type_description',
        'full_name',
        'part_name',
        'lastname',
        'second_lastname',
        'first_name',
        'second_name',
        'birthday',
        'country_id',
        'country',
        'city_name',
        'gender_id',
        'gender',
        'ethnicity_id',
        'ethnicity',
        'department_id',
        'city_id'
    ];

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * User Credentials
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function access()
    {
        return $this->belongsTo(Access::class, 'id');
    }


}
