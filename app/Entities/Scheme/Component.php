<?php

namespace IDRD\Entities\Scheme;

use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as Auditor;

class Component extends Model implements Auditable
{
    use SoftDeletes, CascadeSoftDeletes, Auditor;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'components';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'thematic_id' ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'thematic_id'   => 'int',
    ];

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [ 'name', 'thematic_id' ];

    /**
     * Generating tags for each model audited.
     *
     * @return array
     */
    public function generateTags() : array
    {
        return ['component'];
    }

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator
     * ---------------------------------------------------------
     */

    /**
     * Set name to uppercase
     *
     * @param string $value
     */
    public function setNameAttribute( $value )
    {
        $this->attributes['name'] = toUpper($value);
    }

    /*
    * ---------------------------------------------------------
    * Eloquent Relations
    * ---------------------------------------------------------
    */

    public function thematic()
    {
      return $this->belongsTo(Thematic::class);
    }

    public function strategies()
    {
        return $this->hasMany( Strategy::class, 'component_id', 'id' );
    }
}
