<?php

namespace IDRD\Entities\Scheme;

use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable as Auditor;
use OwenIt\Auditing\Contracts\Auditable;

class SpecificPopulationCharacteristic extends Model implements Auditable
{
    use SoftDeletes, CascadeSoftDeletes, Auditor;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'specific_characteristics';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'population_id' ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
      'population_id'   => 'int',
    ];

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [ 'name', 'population_id' ];

    /**
     * Generating tags for each model audited.
     *
     * @return array
     */
    public function generateTags() : array
    {
      return ['specific_characteristics'];
    }

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator
     * ---------------------------------------------------------
     */

    /**
     * Set name to uppercase
     *
     * @param string $value
     */
    public function setNameAttribute( $value )
    {
      $this->attributes['name'] = toUpper($value);
    }

    /*
    * ---------------------------------------------------------
    * Eloquent Relations
    * ---------------------------------------------------------
    */

    public function population()
    {
      return $this->belongsTo( PopulationCharacteristic::class, 'population_id', 'id' );
    }
}
