<?php

namespace IDRD\Entities\Scheme;

use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable as Auditor;
use OwenIt\Auditing\Contracts\Auditable;

class Neighborhood extends Model implements Auditable
{
  use SoftDeletes, CascadeSoftDeletes, Auditor;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'neighborhoods';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [ 'name', 'upz_id' ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'upz_id'   => 'int',
  ];

  /**
   * Attributes to include in the Audit.
   *
   * @var array
   */
  protected $auditInclude = [ 'name', 'upz_id' ];


  /**
   * Generating tags for each model audited.
   *
   * @return array
   */
  public function generateTags() : array
  {
    return ['neighborhood'];
  }


  /*
   * ---------------------------------------------------------
   * Accessors and Mutator
   * ---------------------------------------------------------
   */

  /**
   * Set name to uppercase
   *
   * @param string $value
   */
  public function setNameAttribute( $value )
  {
    $this->attributes['name'] = toUpper($value);
  }

  /*
  * ---------------------------------------------------------
  * Eloquent Relations
  * ---------------------------------------------------------
  */

  public function upz()
  {
    return $this->belongsTo(Upz::class);
  }
}
