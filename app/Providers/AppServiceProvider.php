<?php

namespace IDRD\Providers;

use IDRD\Entities\Scheme\Activity;
use IDRD\Entities\Scheme\Component;
use IDRD\Entities\Scheme\Location;
use IDRD\Entities\Scheme\Neighborhood;
use IDRD\Entities\Scheme\PopulationCharacteristic;
use IDRD\Entities\Scheme\Program;
use IDRD\Entities\Scheme\SpecificPopulationCharacteristic;
use IDRD\Entities\Scheme\Strategy;
use IDRD\Entities\Scheme\SubActivity;
use IDRD\Entities\Scheme\Thematic;
use IDRD\Entities\Scheme\Upz;
use IDRD\Entities\Security\Permission;
use IDRD\Entities\Security\Role;
use IDRD\Entities\Security\User;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*
         * Prevent MySQL key length error
         */
        Schema::defaultStringLength(191);
        /**
         * Custom Polymorphic Types
         */
        Relation::morphMap([
             //Security
             'users'              =>  User::class,
             'roles'              =>  Role::class,
             'permissions'        =>  Permission::class,
             //Scheme
             'program'            =>  Program::class,
             'activity'           =>  Activity::class,
             'sub_activity'       =>  SubActivity::class,
             'thematic'           =>  Thematic::class,
             'component'          =>  Component::class,
             'strategy'           =>  Strategy::class,
             //Population
             'population'         =>  PopulationCharacteristic::class,
             'specific_population'=>  SpecificPopulationCharacteristic::class,
             //Location
             'location'           => Location::class,
             'upz'                => Upz::class,
             'neighborhood'       => Neighborhood::class,
        ]);
    }
}
