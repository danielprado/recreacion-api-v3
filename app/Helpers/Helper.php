<?php

use IDRDApp\Entities\Votes\Campaign;
use IDRDApp\Entities\Votes\Candidate;

if ( !function_exists('toUpper') ) {
    /**
     * The method to return upper string including spanish chars
     *
     * @param null $string
     * @return mixed|string
     */
    function toUpper( $string = null )
    {
        if ( is_string($string) || is_numeric( $string ) ) {
            return mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_UPPER, 'UTF-8');
        }

        return null;
    }
}

if ( !function_exists('isAValidDate') ) {
    /**
     * The method to return upper string including spanish chars
     *
     * @param $date
     * @param string $format
     * @return bool
     */
    function isAValidDate( $date, $format = 'Y-m-d' )
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}

if ( ! function_exists('toLower') ) {
    /**
     * The method to return lower string including spanish chars
     *
     * @param null $string
     * @return mixed|string
     */
    function toLower( $string = null )
    {
        if ( is_string($string) || is_numeric( $string ) ) {
            return mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_LOWER, 'UTF-8');
        }

        return null;
    }
}

if ( ! function_exists('toTitle') ) {
    /**
     * The method to return title string including spanish chars
     *
     * @param null $string
     * @return mixed|string
     */
    function toTitle( $string = null )
    {
        if ( is_string($string) || is_numeric( $string ) ) {
            return mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_TITLE, 'UTF-8');
        }

        return null;
    }
}

if ( ! function_exists('toFirstUpper') ) {
    /**
     * The method to return title string including spanish chars
     *
     * @param null $string
     * @return mixed|string
     */
    function toFirstUpper( $string = null )
    {
        if ( is_string($string) || is_numeric( $string ) ) {
            $str = ucfirst( mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_LOWER, 'UTF-8') );
            preg_match_all("/\.\s*\w/", $str, $matches);

            foreach($matches[0] as $match){
                $str = str_replace($match, strtoupper($match), $str);
            }
            return $str;
        }

        return null;
    }
}

if ( ! function_exists( 'sim_crypt' ) ) {
  /**
   * Crypt the password
   *
   * @param $password
   * @return string
   */
  function sim_crypt( $password ) {
    $k = 18;
    $C = '';
    for( $i=0; $i < strlen( $password ) ; $i++ ) $C.=chr((ord( $password [$i])+$k)%255);
    return sha1( $C );
  }
}


if ( ! function_exists('sim_decrypt') ) {
  /**
   * Decrypt password
   *
   * @param $password
   * @return string
   */
  function sim_decrypt($password) {
    $k = 18;
    $M = '';
    for($i=0; $i<strlen($password); $i++)$M.=chr((ord($password[$i])-$k+255)%255);
    return $M;
  }
}
