<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('oauth/token', '\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken');
Route::post('logout', 'Auth\PassportAuthController@logout');

Route::middleware('auth:api')->prefix('api')->group( function () {
    require __DIR__.'/api/security.php';
    require __DIR__.'/api/scheme.php';
});
