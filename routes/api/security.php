<?php
/*
|--------------------------------------------------------------------------
| Security API Routes
|--------------------------------------------------------------------------
*/

Route::namespace('Security')->group(function () {
    Route::get('/user', 'UserController@profile');
    Route::post('/profile/check', 'ProfileController@check');
    Route::post('/access/check', 'AccessController@check');

    Route::resource('access', 'AccessController', [
        'only'     =>   ['store', 'update'],
        'parameters' => [ 'access' => 'access' ]
    ]);
    Route::resource('profile', 'ProfileController', [
        'only'     =>   ['store', 'update'],
        'parameters' => [ 'profile' => 'person' ]
    ]);

    Route::resource('countries.cities', 'CityController', [
        'only'     =>   ['index'],
        'parameters' => [ 'countries' => 'country' ]
    ]);
    Route::resource('countries', 'CountryController', [
        'only'     =>   ['index'],
        'parameters' => [ 'countries' => 'country' ]
    ]);
    Route::resource('genders', 'GenderController', [
        'only'     =>   ['index'],
        'parameters' => [ 'genders' => 'gender' ]
    ]);
    Route::resource('ethnicities', 'EthnicityController', [
        'only'     =>   ['index'],
        'parameters' => [ 'ethnicities' => 'ethnicity' ]
    ]);
    Route::resource('document-types', 'DocumentTypeController', [
        'only'     =>   ['index'],
        'parameters' => [ 'document-types' => 'document_type' ]
    ]);

    Route::post('/massive-access-sim', 'MassiveUserController@massive_access_sim');

    Route::patch('/users/{id}/restore', 'UserController@restore');
    Route::resource('users', 'UserController', [
        'except'     =>   ['create', 'edit'],
        'parameters' => [ 'users' => 'user' ]
    ]);

    Route::get('/users/{user}/assigned-roles', 'UserRolesController@show');
    Route::resource('users.roles', 'UserRolesController', [
        'only'     =>   ['index', 'store'],
        'parameters' => [ 'users' => 'user' ]
    ]);

    Route::resource('roles.permissions', 'RolePermissionController', [
        'only'     =>   ['index', 'store'],
        'parameters' => [ 'roles' => 'role' ]
    ]);



    Route::get('/roles/all', 'RoleController@all');
    Route::patch('/roles/{id}/restore', 'RoleController@restore');
    Route::resource('roles', 'RoleController', [
        'except'     =>   ['create', 'edit'],
        'parameters' => [ 'roles' => 'role' ]
    ]);

    Route::get('/permissions/all', 'PermissionController@all');
    Route::patch('/permissions/{id}/restore', 'PermissionController@restore');
    Route::resource('permissions', 'PermissionController', [
        'except'     =>   ['create', 'edit'],
        'parameters' => [ 'permissions' => 'permission' ]
    ]);
});