<?php
/*
|--------------------------------------------------------------------------
| Scheme API Routes
|--------------------------------------------------------------------------
*/
Route::namespace('Scheme')->group(function () {
    Route::get('programs/all', 'ProgramController@all');
    Route::get('programs/{program}/activities', 'ProgramController@activities');
    Route::patch('programs/{id}/restore', 'ProgramController@restore');
    Route::resource('programs', 'ProgramController', [
        'except'     =>     ['create', 'edit'],
        'parameters' =>     ['programs' => 'program']
    ]);
    Route::get('activities/{activity}/sub-activities', 'ActivityController@sub_activities');
    Route::patch('activities/{id}/restore', 'ActivityController@restore');
    Route::resource('activities', 'ActivityController', [
        'except'     =>     ['create', 'edit'],
        'parameters' =>     ['activities' => 'activity']
    ]);

    Route::get('sub-activities/{sub_activity}/thematics', 'SubActivityController@thematics');
    Route::patch('sub-activities/{id}/restore', 'SubActivityController@restore');
    Route::resource('sub-activities', 'SubActivityController', [
        'except'     =>     ['create', 'edit'],
        'parameters' =>     ['sub-activities' => 'sub_activity']
    ]);

    Route::get('thematics/{thematic}/components', 'ThematicController@components');
    Route::patch('thematics/{id}/restore', 'ThematicController@restore');
    Route::resource('thematics', 'ThematicController', [
        'except'     =>     ['create', 'edit'],
        'parameters' =>     ['thematics' => 'thematic']
    ]);

    Route::get('components/{component}/strategies', 'ComponentController@strategies');
    Route::patch('components/{id}/restore', 'ComponentController@restore');
    Route::resource('components', 'ComponentController', [
        'except'     =>     ['create', 'edit'],
        'parameters' =>     ['components' => 'component']
    ]);

    Route::patch('strategies/{strategy}/restore', 'StrategyController@restore');
    Route::resource('strategies', 'StrategyController', [
        'except'     =>     ['create', 'edit'],
        'parameters' =>     ['strategies' => 'strategy']
    ]);

    Route::get('population-characteristics/all', 'PopulationCharacteristicController@all');
    Route::get('population-characteristics/{population}/specifics', 'PopulationCharacteristicController@specifics');
    Route::patch('population-characteristics/{id}/restore', 'PopulationCharacteristicController@restore');
    Route::resource('population-characteristics', 'PopulationCharacteristicController', [
        'except'     =>     ['create', 'edit'],
        'parameters' =>     ['population-characteristics' => 'population']
    ]);

    Route::patch('specific-population-characteristics/{id}/restore', 'PopulationCharacteristicController@restore');
    Route::resource('specific-population-characteristics', 'PopulationCharacteristicController', [
        'except'     =>     ['create', 'edit'],
        'parameters' =>     ['specific-population-characteristics' => 'specific_population']
    ]);

    Route::get('locations/{location}/upz', 'LocationController@upz');
    Route::patch('locations/{id}/restore', 'LocationController@restore');
    Route::resource('locations', 'LocationController', [
      'except'     =>     ['create', 'edit'],
      'parameters' =>     ['locations' => 'location']
    ]);

    Route::get('upz/{upz}/neighborhoods', 'UpzController@neighborhoods');
    Route::patch('upz/{id}/restore', 'UpzController@restore');
    Route::resource('upz', 'UpzController', [
      'except'     =>     ['create', 'edit'],
      'parameters' =>     ['upz' => 'upz']
    ]);

    Route::patch('neighborhoods/{id}/restore', 'NeighborhoodController@restore');
    Route::resource('neighborhoods', 'NeighborhoodController', [
      'except'     =>     ['create', 'edit'],
      'parameters' =>     ['neighborhoods' => 'neighborhood']
    ]);
});
