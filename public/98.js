((typeof self !== 'undefined' ? self : this)["webpackJsonp"] = (typeof self !== 'undefined' ? self : this)["webpackJsonp"] || []).push([[98],{

/***/ "./resources/src/models/services/Security/Role.js":
/*!********************************************************!*\
  !*** ./resources/src/models/services/Security/Role.js ***!
  \********************************************************/
/*! exports provided: Role */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Role\", function() { return Role; });\n/* harmony import */ var _models_Model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/models/Model */ \"./resources/src/models/Model.js\");\n/* harmony import */ var _models_Api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/models/Api */ \"./resources/src/models/Api.js\");\n\n\n\nclass Role extends _models_Model__WEBPACK_IMPORTED_MODULE_0__[\"Model\"] {\n  constructor(data = {\n    name: null,\n    display_name: null,\n    description: null\n  }) {\n    super(_models_Api__WEBPACK_IMPORTED_MODULE_1__[\"Api\"].END_POINTS.ROLES(), data);\n  }\n\n  all(options = {}) {\n    return this.get(`${this.url}/all`, options)\n  }\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZXNvdXJjZXMvc3JjL21vZGVscy9zZXJ2aWNlcy9TZWN1cml0eS9Sb2xlLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL3NyYy9tb2RlbHMvc2VydmljZXMvU2VjdXJpdHkvUm9sZS5qcz83YjhlIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TW9kZWx9IGZyb20gXCJAL21vZGVscy9Nb2RlbFwiO1xuaW1wb3J0IHtBcGl9IGZyb20gXCJAL21vZGVscy9BcGlcIjtcblxuZXhwb3J0IGNsYXNzIFJvbGUgZXh0ZW5kcyBNb2RlbCB7XG4gIGNvbnN0cnVjdG9yKGRhdGEgPSB7XG4gICAgbmFtZTogbnVsbCxcbiAgICBkaXNwbGF5X25hbWU6IG51bGwsXG4gICAgZGVzY3JpcHRpb246IG51bGxcbiAgfSkge1xuICAgIHN1cGVyKEFwaS5FTkRfUE9JTlRTLlJPTEVTKCksIGRhdGEpO1xuICB9XG5cbiAgYWxsKG9wdGlvbnMgPSB7fSkge1xuICAgIHJldHVybiB0aGlzLmdldChgJHt0aGlzLnVybH0vYWxsYCwgb3B0aW9ucylcbiAgfVxufSJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./resources/src/models/services/Security/Role.js\n");

/***/ })

}]);