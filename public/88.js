((typeof self !== 'undefined' ? self : this)["webpackJsonp"] = (typeof self !== 'undefined' ? self : this)["webpackJsonp"] || []).push([[88],{

/***/ "./resources/src/models/services/Commitment.js":
/*!*****************************************************!*\
  !*** ./resources/src/models/services/Commitment.js ***!
  \*****************************************************/
/*! exports provided: Commitment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Commitment\", function() { return Commitment; });\n/* harmony import */ var _models_Model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/models/Model */ \"./resources/src/models/Model.js\");\n/* harmony import */ var _models_Api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/models/Api */ \"./resources/src/models/Api.js\");\n\n\n\nclass Commitment extends _models_Model__WEBPACK_IMPORTED_MODULE_0__[\"Model\"] {\n  constructor(id, data = {\n    responsable: null,\n    description: null,\n    date: null\n  }) {\n    super(_models_Api__WEBPACK_IMPORTED_MODULE_1__[\"Api\"].END_POINTS.PROGRAMMINGS_COMMITMENTS(id), data);\n  }\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZXNvdXJjZXMvc3JjL21vZGVscy9zZXJ2aWNlcy9Db21taXRtZW50LmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL3NyYy9tb2RlbHMvc2VydmljZXMvQ29tbWl0bWVudC5qcz9kZmFkIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TW9kZWx9IGZyb20gXCJAL21vZGVscy9Nb2RlbFwiO1xuaW1wb3J0IHtBcGl9IGZyb20gXCJAL21vZGVscy9BcGlcIjtcblxuZXhwb3J0IGNsYXNzIENvbW1pdG1lbnQgZXh0ZW5kcyBNb2RlbCB7XG4gIGNvbnN0cnVjdG9yKGlkLCBkYXRhID0ge1xuICAgIHJlc3BvbnNhYmxlOiBudWxsLFxuICAgIGRlc2NyaXB0aW9uOiBudWxsLFxuICAgIGRhdGU6IG51bGxcbiAgfSkge1xuICAgIHN1cGVyKEFwaS5FTkRfUE9JTlRTLlBST0dSQU1NSU5HU19DT01NSVRNRU5UUyhpZCksIGRhdGEpO1xuICB9XG59Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./resources/src/models/services/Commitment.js\n");

/***/ })

}]);