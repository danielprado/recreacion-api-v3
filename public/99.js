((typeof self !== 'undefined' ? self : this)["webpackJsonp"] = (typeof self !== 'undefined' ? self : this)["webpackJsonp"] || []).push([[99],{

/***/ "./resources/src/models/services/Tracing.js":
/*!**************************************************!*\
  !*** ./resources/src/models/services/Tracing.js ***!
  \**************************************************/
/*! exports provided: Tracing */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Tracing\", function() { return Tracing; });\n/* harmony import */ var _models_Model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/models/Model */ \"./resources/src/models/Model.js\");\n/* harmony import */ var _models_Api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/models/Api */ \"./resources/src/models/Api.js\");\n\n\n\nclass Tracing extends _models_Model__WEBPACK_IMPORTED_MODULE_0__[\"Model\"] {\n  constructor(programming_id, commitment_id, data = {\n    date: null,\n    description: null\n  }) {\n    super(_models_Api__WEBPACK_IMPORTED_MODULE_1__[\"Api\"].END_POINTS.PROGRAMMINGS_COMMITMENTS_TRACINGS(programming_id, commitment_id), data );\n  }\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZXNvdXJjZXMvc3JjL21vZGVscy9zZXJ2aWNlcy9UcmFjaW5nLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL3NyYy9tb2RlbHMvc2VydmljZXMvVHJhY2luZy5qcz8xZDg0Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TW9kZWx9IGZyb20gXCJAL21vZGVscy9Nb2RlbFwiO1xuaW1wb3J0IHtBcGl9IGZyb20gXCJAL21vZGVscy9BcGlcIjtcblxuZXhwb3J0IGNsYXNzIFRyYWNpbmcgZXh0ZW5kcyBNb2RlbCB7XG4gIGNvbnN0cnVjdG9yKHByb2dyYW1taW5nX2lkLCBjb21taXRtZW50X2lkLCBkYXRhID0ge1xuICAgIGRhdGU6IG51bGwsXG4gICAgZGVzY3JpcHRpb246IG51bGxcbiAgfSkge1xuICAgIHN1cGVyKEFwaS5FTkRfUE9JTlRTLlBST0dSQU1NSU5HU19DT01NSVRNRU5UU19UUkFDSU5HUyhwcm9ncmFtbWluZ19pZCwgY29tbWl0bWVudF9pZCksIGRhdGEgKTtcbiAgfVxufSJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./resources/src/models/services/Tracing.js\n");

/***/ })

}]);