((typeof self !== 'undefined' ? self : this)["webpackJsonp"] = (typeof self !== 'undefined' ? self : this)["webpackJsonp"] || []).push([[93],{

/***/ "./resources/src/models/services/Scheme/Strategy.js":
/*!**********************************************************!*\
  !*** ./resources/src/models/services/Scheme/Strategy.js ***!
  \**********************************************************/
/*! exports provided: Strategy */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Strategy\", function() { return Strategy; });\n/* harmony import */ var _models_Model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/models/Model */ \"./resources/src/models/Model.js\");\n/* harmony import */ var _models_Api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/models/Api */ \"./resources/src/models/Api.js\");\n\n\n\nclass Strategy extends _models_Model__WEBPACK_IMPORTED_MODULE_0__[\"Model\"] {\n  constructor(data = {\n    name: null,\n    component_id: null,\n    thematic_id: null,\n    sub_activity_id: null,\n    activity_id: null,\n    program_id: null,\n  }) {\n    super(_models_Api__WEBPACK_IMPORTED_MODULE_1__[\"Api\"].END_POINTS.STRATEGIES(), data);\n  }\n}\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZXNvdXJjZXMvc3JjL21vZGVscy9zZXJ2aWNlcy9TY2hlbWUvU3RyYXRlZ3kuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvc3JjL21vZGVscy9zZXJ2aWNlcy9TY2hlbWUvU3RyYXRlZ3kuanM/YTc5YiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge01vZGVsfSBmcm9tIFwiQC9tb2RlbHMvTW9kZWxcIjtcbmltcG9ydCB7QXBpfSBmcm9tIFwiQC9tb2RlbHMvQXBpXCI7XG5cbmV4cG9ydCBjbGFzcyBTdHJhdGVneSBleHRlbmRzIE1vZGVsIHtcbiAgY29uc3RydWN0b3IoZGF0YSA9IHtcbiAgICBuYW1lOiBudWxsLFxuICAgIGNvbXBvbmVudF9pZDogbnVsbCxcbiAgICB0aGVtYXRpY19pZDogbnVsbCxcbiAgICBzdWJfYWN0aXZpdHlfaWQ6IG51bGwsXG4gICAgYWN0aXZpdHlfaWQ6IG51bGwsXG4gICAgcHJvZ3JhbV9pZDogbnVsbCxcbiAgfSkge1xuICAgIHN1cGVyKEFwaS5FTkRfUE9JTlRTLlNUUkFURUdJRVMoKSwgZGF0YSk7XG4gIH1cbn1cbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./resources/src/models/services/Scheme/Strategy.js\n");

/***/ })

}]);